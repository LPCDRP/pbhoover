from setuptools import setup, find_packages
from Cython.Build import cythonize

setup(
    name = 'pbhoover',
    license = 'LICENSE',
    packages = find_packages('.'),
    package_dir = {'':'.'},
    include_dirs=['pbhoover'],
    zip_safe = False,
    entry_points = dict(console_scripts=[
        'pbhoover = pbhoover.pbhoover:main',
    ]),
    ext_modules=cythonize('*/*.pyx',
                            compiler_directives={'language_level': "3"}),
    setup_requires=['pytest-runner', ],
    tests_require=['pytest'],
    test_suite='pbhoover.test.test_pbhoover'
)
