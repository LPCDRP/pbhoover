
import numpy as np
import h5py
import logging

__author__ = 'Sarah Ramirez-Busby'


def getReadsInterval(readLocaterByKey, h5, referenceLength, referenceKey, numberofChunks, chunkSize, mapQV):
    """author: Sarah Ramirez-Busby
    Based on the input region size and how many regions are in the reference genome, this will
    identify optimal cut-off points for each interval. A deletion or insertion could overlap
    the end of the input range. THis will affect downstream consensus calling. Therefore,
    the end of each range is checked for a deletion or insertion and extending the input range
    by the number of bases involved in the indel.

    :param h5: an h5py File object of a cmp.h5 alignment file produced by BLASR/samtoh5
    :param referenceLength: the length in base pairs of the input reference sequence
    :param referenceKey: the first index of the reference FASTA header, split by whitespace
    :param numberofChunks: the number of regions that need processing
    :param chunkSize: the size of each region to process
    :param mapQV: the minimum mapping quality threshold to accept a read
    :return intervalPoints: list of tuples containing start and end of regions
    """
    intervalPoints = []
    for chunkNum in range(0, numberofChunks):
        inRangeStart = chunkSize * chunkNum
        inRangeEnd = chunkSize * chunkNum + 1 + chunkSize
        readsAtCutPoint = readsInRange(refRange=(inRangeStart, inRangeEnd),
                                       readLocatorDict=readLocaterByKey,
                                       readInfoDict=h5,
                                       mapQv=mapQV,
                                       refKey=referenceKey)
        deletionBaseCounts = []
        insertionBaseCounts = []
        # Checking if any reads end in a deletion
        for readInfo in readsAtCutPoint[1]:
            strand = readInfo[2]
            referenceRead = referenceFromAlignmentArray(in_array=readInfo[0], complement=strand).decode('UTF-8')
            tStart = readInfo[1][0]
            read = readFromAlignmentArray(in_array=readInfo[0], complement=strand).decode('UTF-8')
            if strand:
                reversedread = read[::-1]
            else:
                reversedread = read
            # Get a sequence of reference positions that the read overlaps
            refPositions = [p for p in range(tStart, tStart + len(read))]
            insertionPositions = [refPositions[i] for i, b in enumerate(referenceRead) if '-' in b or b == '-']
            hasInsertion = False
            if inRangeEnd in insertionPositions:
                hasInsertion = True
            # If the end of the input range is in the reads that overlap reference positions
            delNextBaseCount = 0
            if inRangeEnd in refPositions:
                refEndPosIndex = refPositions.index(inRangeEnd)
                deletion = True
                while deletion is True:
                    base = reversedread[refEndPosIndex + delNextBaseCount]
                    # If the current base is a deletion or contains a deletion
                    # containment catches all cases, the string cannot meet the
                    # exact match
                    if base == '-' or '-' in base:
                        delNextBaseCount += 1
                    else:
                        deletion = False
                deletionBaseCounts.append(delNextBaseCount)
                if hasInsertion:
                    insNextBaseCount = len(insertionPositions)
                    insertionBaseCounts.append(insNextBaseCount)
        try:
            maxDelBaseCount = max(deletionBaseCounts)
        except ValueError:
            maxDelBaseCount = 0
        try:
            maxInsBaseCount = max(insertionBaseCounts)
        except ValueError:
            maxInsBaseCount = 0
        basesToAdd = maxDelBaseCount + maxInsBaseCount + 1
        if len(intervalPoints) == 0:
            intervalPoints.append((0, inRangeEnd + basesToAdd))
        else:
            # Subtract 2, 1 for the 0-based index, 1 for the previous element
            intervalPoints.append((intervalPoints[chunkNum - 2][1], inRangeStart + basesToAdd))
    intervalPoints.append((intervalPoints[-1][1], referenceLength))
    return intervalPoints


def readsInRange(refRange, readLocatorDict, refKey, readInfoDict, mapQv=20):
    """
    author: Sarah Ramirez-Busby
    Based on an input range, identify all reads overlapping the positions and provide the read and reference bases,
    the quality values (including deletion QV), the reference start, end of the read, and the strand.

    example: readsInRange(cmph5,'gi|448814763|ref|NC_000962.3|', (1708248, 1709410), 20)

    :param cmph5: h5py File object
    :param refKey: reference ID in the FASTA header (first index split by whitespace)
    :param refRange: tuple containing the desired start and end
    :param minimumMapQV: minimum mapping quality to include the read
    :return: list containing the input refRange, full read information, deletion and QV,
             mapping qualities, and the read
    """
    logger = logging.getLogger('readsInRange')
    readsMetricsList = ['', '']
    if refRange[0] > refRange[1]:
        refRange = (refRange[1], refRange[0])
    overlappingRefs = []
    filtered_reads = readLocatorDict[refKey](refRange[0], refRange[1], justIndices=False)
    for read in filtered_reads:
        if read[13] > mapQv:
            overlappingRefs.append(parse_read(read, readInfoDict))
    readsMetricsList[0] = refRange
    readsMetricsList[1] = overlappingRefs
    return readsMetricsList


def parse_read(read, cmph5Dict):
    readOffSet = (read[18], read[19])
    strand = read[6]
    path = cmph5Dict['AlnGroup']['Path'][0].decode('UTF-8')
    keys = path.split('/')
    readInfo = cmph5Dict[keys[1]][keys[2]]
    alignmentArray = readInfo['AlnArray'][readOffSet]
    delQV = readInfo['DeletionQV'][readOffSet]
    QV = readInfo['QualityValue'][readOffSet]
    tStart = int(read[4])
    tEnd = tStart + len(alignmentArray)
    overlappingRefPositions = (tStart, tEnd)
    return [alignmentArray, overlappingRefPositions, strand, QV, delQV]



def pulseFeature(h5pyfile, featureName, offSet):
    """
    author: David Alexander
    modifications: Sarah Ramirez-Busby

    Retrieves all the quality metrics for a given read

    :param h5pyfile: h5py file object
    :param featureName: name of feature to retreive
    :param offSet: reference position offset
    :return: array of feature information
    """
    # Get H5 structure to AlnArray
    path = h5pyfile['AlnGroup']['Path'][()][0]
    pulseDataset = h5pyfile[path][featureName]
    pulseArray = arrayFromDataset(pulseDataset, offSet[0], offSet[1])
    return pulseArray


def arrayFromDataset(ds, offsetBegin, offsetEnd):
    """
    author: David Alexander

    Extract a one-dimensional array from an HDF5 dataset.

    :param ds: input data
    :param offsetBegin: start of the off set
    :param offsetEnd: end of the off set
    :return: array
    """
    shape = (offsetEnd - offsetBegin,)
    a = np.ndarray(shape=shape, dtype=ds.dtype)
    mspace = h5py.h5s.create_simple(shape)
    fspace = ds.id.get_space()
    fspace.select_hyperslab((offsetBegin,), shape, (1,))
    ds.id.read(mspace, fspace, a)
    return a


_basemap = {0b0000: ord("-"),
            0b0001: ord("A"),
            0b0010: ord("C"),
            0b0100: ord("G"),
            0b1000: ord("T"),
            0b1111: ord("N")}
_cBasemap = {0b0000: ord("-"),
             0b0001: ord("T"),
             0b0010: ord("G"),
             0b0100: ord("C"),
             0b1000: ord("A"),
             0b1111: ord("N")}
_basemapArray = np.ndarray(shape=(max(_basemap.keys()) + 1,), dtype=np.byte)
_cBasemapArray = np.ndarray(shape=(max(_basemap.keys()) + 1,), dtype=np.byte)
for (e, v) in _basemap.items():
    _basemapArray[e] = v
for (e, v) in _cBasemap.items():
    _cBasemapArray[e] = v


def referenceFromAlignmentArray(in_array, complement=False):
    """
    author: David Alexander
    modifications: Sarah Ramirez-Busby

    Decode the reference component of an alignment array.

    :param in_array: array to convert to strings
    :param complement: if the complement bases should be considered
    :return: string of bases
    """
    if complement:
        r = _cBasemapArray[in_array & 0b1111]
    else:
        r = _basemapArray[in_array & 0b1111]
    return r.tostring()


def readFromAlignmentArray(in_array, complement=False):
    """
    author: David Alexander
    modifications: Sarah Ramirez-Busby

    Decode the read component of an alignment array.
    returns a string of bases

    :param in_array: array to convert to strings
    :param complement: if the complement bases should be considered
    :return: string of bases
    """
    if complement:
        r = _cBasemapArray[in_array >> 4]
    else:
        r = _basemapArray[in_array >> 4]

    return r.tostring()
