from __future__ import division
import operator
from operator import itemgetter
from math import log
from functools import reduce
import sys
import logging
import pysam
import itertools
from skbio import DNA
from skbio.alignment import local_pairwise_align_ssw
from collections import OrderedDict, Counter


def initVCF(outfile, consensus):
    """
    Initializes an output VCF file

    :param outfile: output VCF file name
    :return: None
    """

    from _version import version as __version__
    vcf_template = OrderedDict()
    vcf_template['fileformat'] = 'VCFv4.0'
    vcf_template['source'] = 'pbhooverV' + __version__
    vcf_template['INFO'] = [['DP', 1, 'Integer', 'Read depth']]
    vcf_template['INFO'] += [['AF', '.', 'String', 'Allele frequencies for all base calls '
                                                   'Format: Base:Proportion of DP']]
    vcf_template['INFO'] += [['MV', '.', 'String', 'Minor variant calls and corresponding quality. '
                                                              'Format: Base:Quality']]
    if consensus is True:
        vcf_template['FILTER'] = [['LOW', 'Position with too low of depth']]
        vcf_template['FILTER'] += [['NO', 'Reference call (does not meet criteria to call a variant)']]
        vcf_template['FILTER'] += [['HETERO', 'Enough support to call reference and top variant (mixed population)']]
    else:
        vcf_template['FILTER'] = [['HETERO', 'Enough support to call reference and top variant (mixed population)']]
    vcf_template['column_header'] = ['#CHROM', 'POS', 'ID', 'REF', 'ALT', 'QUAL', 'FILTER', 'INFO']
    with open(outfile, 'w') as filehandle:
        for h, descript in vcf_template.items():
            if h in ['fileformat', 'source']:
                filehandle.write(''.join(['##', h, '=', descript]) + '\n')
            elif h == 'INFO':
                for d in descript:
                    filehandle.write(''.join(['##', h,
                                              '=<ID=', d[0],
                                              ',Number=', str(d[1]),
                                              ',Type=', d[2],
                                              ',Description="', d[3],
                                              '">']) + '\n')
            elif h == 'FILTER':
                for d in descript:
                    filehandle.write(''.join(['##', h,
                                              '=<ID=', d[0],
                                              ',Description="', d[1],
                                              '">']) + '\n')
            elif h == 'column_header':
                filehandle.write('\t'.join(descript) + '\n')


def calculateConsensusQuality(in_qualities):
    """
    Calculates the quality of the consensus base call

    :param in_qualities: dictionary of key: base, value: list of base qualities
    :return: consensus quality dictionary key: consensus base, value: consensus quality
    """
    in_qualities = {k: v for k, v in in_qualities.items() if v != []}
    quality_calc = {}
    for call, quality in in_qualities.items():
        # Values are lists, getting all lists returns a list of lists
        opposing_qualities = sum([l for k, v in in_qualities.items() for l in v if k != call])
        if not opposing_qualities:
            product = reduce(operator.mul, [10 ** (-float(opposing_qualities) / 10)])
        else:
            product = 0
        if product >= 1:
            quality_calc[call] = 0
        else:
            quality_calc[call] = sum(quality) - 10 * log(1 - product, 10)
    return quality_calc


def is_insertion(call):
    """
    Identifies an insertion

    :param call: base
    :return: bool
    """
    if len(call) > 1 and not call.startswith('-'):
        return True
    return False


def is_deletion(call):
    """
    Identifies a deletion

    :param call: base
    :return: bool
    """
    return call[0] == '-'


def is_multibase_indel(call):
    """
    Identifies a multibase indel

    :param call: base
    :return: bool
    """
    return len(call) > 2


def is_single_indel(call):
    """
    Identifies a single base indel

    :param call: base
    :return: bool
    """
    return len(call) == 2


def is_snp(call):
    """
    Identifies a nucleotide substitution

    :param call: base
    :return: bool
    """
    return len(call) == 1 and not is_deletion(call)


def collapse_indels(in_variants):
    deletions = [i.lstrip('-') for i in in_variants.keys() if i.startswith('-')]
    insertions = [i.lstrip('+') for i in in_variants.keys() if i.startswith('+')]
    updated_variants = {}
    # If there are deletions
    if len(deletions) > 1:
        alignments = []
        for pair in list(itertools.combinations(deletions, 2)):
            if len(pair[0]) == 1 and len(pair[1]) == 1:
                continue
            try:
                alignment, score, table = local_pairwise_align_ssw(DNA(str(pair[0])), DNA(str(pair[1])))
            except IndexError:
                continue
            alignments.append((pair[0], pair[1], str(alignment[0]), len(str(alignment[0]))))
        readsToUpdate = {}
        for a in alignments:
            read1 = '-' + a[0]
            read2 = '-' + a[1]
            updatedRead = '-' + a[2]
            alignmentLength = a[3]
            if read1 in readsToUpdate.keys():
                if len(readsToUpdate[read1]) > alignmentLength:
                    readsToUpdate[read1] = updatedRead
            else:
                readsToUpdate[read1] = updatedRead
            if read2 in readsToUpdate.keys():
                if len(readsToUpdate[read2]) > alignmentLength:
                    readsToUpdate[read2] = updatedRead
            else:
                readsToUpdate[read2] = updatedRead
        for base, qvs in in_variants.items():
            if base in readsToUpdate.keys():
                try:
                    updated_variants[readsToUpdate[base]] += qvs
                except KeyError:
                    updated_variants[readsToUpdate[base]] = qvs
            else:
                updated_variants[base] = qvs
    # If there are insertions
    if len(insertions) > 1:
        alignments = []
        for pair in list(itertools.combinations(insertions, 2)):
            if len(pair[0]) == 1 and len(pair[1]) == 1:
                continue
            try:
                alignment, score, table = local_pairwise_align_ssw(DNA(str(pair[0])), DNA(str(pair[1])))
            except IndexError:
                continue
            alignments.append((pair[0], pair[1], str(alignment[0]), len(str(alignment[0]))))
        readsToUpdate = {}
        for a in alignments:
            read1 = '+' + a[0]
            read2 = '+' + a[1]
            updatedRead = '+' + a[2]
            alignmentLength = a[3]
            if read1 in readsToUpdate.keys():
                if len(readsToUpdate[read1]) > alignmentLength:
                    readsToUpdate[read1] = updatedRead
            else:
                readsToUpdate[read1] = updatedRead
            if read2 in readsToUpdate.keys():
                if len(readsToUpdate[read2]) > alignmentLength:
                    readsToUpdate[read2] = updatedRead
            else:
                readsToUpdate[read2] = updatedRead
        for base, qvs in in_variants.items():
            if base in readsToUpdate.keys():
                try:
                    updated_variants[readsToUpdate[base]] += qvs
                except KeyError:
                    updated_variants[readsToUpdate[base]] = qvs
            else:
                updated_variants[base] = qvs
    # If there are no indels
    if not updated_variants:
        return in_variants
    return updated_variants


def consensus(referencecall, position, calls, qualities):
    """
    Calls the consensus base for a given position

    :param referencecall: reference base
    :param position: current position
    :param calls: dictionary of bases called at the position with coverage of each call
    :param qualities: dictionary of bases at the position with qualities of each call
    :param multi_del: if a call has a multi-base deletion
    :return: list of the position, reference, alternative allele,
    consensus quality, and variant information
    """
    refbase = referencecall
    var_calls = {call: coverage for call, coverage in
                 calls.items() if call != refbase and coverage != 0}
    alleleFreqs = calculateAlleleFrequencies(calls, sum(calls.values()))
    # If there are no reads at this position
    if not var_calls.keys():
        if calls.keys():
            RSR = calls[refbase]
            FILTER = 'NO'
        else:
            RSR = 0
            FILTER = 'LOW'
        VSR = 0
        var_freq = 0.0
        POS = position
        ALT = ''
        QUAL = 0.0
        DP = 0
        INFO = dict(AF=','.join([':'.join([base, str(f)]) for base, f in alleleFreqs.items()]),
                    DP=DP,
                    MV='')
    else:
        var_quals = {call: qual for call, qual in
                     qualities.items() if call != refbase and call in var_calls.keys()}
        variant = getTopVariant(variant_coverage=var_calls,
                                qualities=var_quals)
        minorVariants = getMinorVariants(topVariant=variant,
                                         variantQualities=var_quals)
        RSR = calls[refbase] if refbase in calls else 0
        VSR = calls[variant]
        var_freq = VSR/(RSR + VSR)
        POS = position
        DP = sum(calls.values())
        # If the top variant is an insertion
        if is_insertion(variant):
            ALT = refbase + variant[1:]
        else:
            ALT = variant
        QUAL = "%.2f" % qualities[variant]
        INFO = dict(DP=DP,
                    AF=','.join([':'.join([base, str(f)]) for base, f in alleleFreqs.items()]),
                    MV=','.join([':'.join([base, str(float(qv))]) for base, qv in minorVariants.items()]))
        FILTER = evaluateVariant(variant=variant,
                                 RSR=RSR,
                                 VSR=VSR,
                                 var_freq=var_freq)
    REF = refbase
    return [POS, REF, ALT, QUAL, FILTER, INFO]


def createVCFRecord(in_list, refHeader):
    """
    Creates a VCF record

    :param in_list: line to prepare for VCF
    :param refHeader: chromosome/reference header
    :return: list in VCF format
    """
    CHROM = refHeader
    POS = in_list[0]
    ID = '.'
    REF = str(in_list[1])
    ALT = in_list[2] if in_list[2] != 'None' else '.'
    QUAL = str(in_list[3].replace('.00', '.0')) if type(in_list[3]) is str and '.00' in in_list[3] else str(in_list[3])
    FILTER = ''.join(in_list[4])
    INFO = ';'.join(['='.join([k, str(v)]) for k, v in in_list[5].items()]) if type(in_list[5]) is dict else in_list[5]
    vcf_entry = [CHROM, POS, ID, REF, ALT, QUAL, FILTER, INFO]
    return vcf_entry


def writeVCF(output, results, consensus):
    """
    Writes a VCF file

    :param output: file name
    :param results: list of variants called
    :param consensus: lists all bases in the output if True, only variants if false
    :return: None
    """
    initVCF(output, consensus)
    out_lines = []
    for chunk in results:
        if type(chunk[0]) is list:
            for vcfline in chunk:
                if type(vcfline[0]) is list:
                    for l in vcfline:
                        if not l or (consensus is False and l[6] in ['NO', 'LOW']):
                            continue
                        out_lines.append([str(i) for i in l])
                else:
                    if not vcfline or (consensus is False and vcfline[6] in ['NO', 'LOW']):
                        continue
                    out_lines.append([str(i) for i in vcfline])
        else:
            if not chunk or (consensus is False and chunk[6] in ['NO', 'LOW']):
                continue
            out_lines.append([str(i) for i in chunk])
    with open(output, 'a') as out:
        for line in out_lines:
            out.write('\t'.join(line) + '\n')


def normalize(results_list, reference, reference_header, consensus):
    """
    Left-aligns indels using bcftools norm

    :param results_list: list of variants
    :param reference: reference FASTA
    :param nproc: number of processors
    :return: list of normalized variants
    """
    logger = logging.getLogger('Normalization')
    logger.info('Left aligning deletions')
    pysam_reference = pysam.FastaFile(reference)

    def norm(pysam_fasta, chrom, pos, ref, alt):
        """
        A python implementation of the algorithm for variant
        normalization described by Tan et al 2015:
            Tan A, Abecasis GR, Kang HM. Unified representation of genetic variants.
            Bioinformatics. 2015 Jul 1;31(13):2202-4. doi: 10.1093/bioinformatics/btv112.
            Epub 2015 Feb 19. PubMed PMID: 25701572
        Accepts a pysam FastaFile object pointing to the reference genome, and
        chrom, pos, ref, alt genomic coordinates, and normalizes them.

        https://github.com/ericminikel/minimal_representation/blob/master/normalize.py

        :param pysam_fasta: psyam FastaFile object
        :param chrom: chromosome
        :param pos: position
        :param ref: reference base
        :param alt: alternative allele
        :return: list of normalized input
        """
        pos = int(float(pos))  # make sure position is an integer
        ref = ref.upper() if ref is not list else ref[0].upper()
        alt = alt.upper()
        # use blanks instead of hyphens
        if ref == '-':
            ref = ''
        if alt == '-':
            alt = ''
        # check whether the REF is correct
        true_ref = pysam_fasta.fetch(chrom, pos - 1, pos - 1 + len(ref)).upper()
        if ref != true_ref:
            logger.error(
                'Incorrect REF value: %s %s %s %s (actual REF should be %s)' % (chrom, pos, ref, alt, true_ref))
        # Prevent infinte loops in cases where REF == ALT.
        # We have encountered this error in genomic coordinates from the ClinVar XML file
        if ref == alt and consensus is False:
            logger.error('The REF and ALT allele are the same: %s %s %s %s' % (chrom, pos, ref, alt))
        # Time-saving shortcut for SNPs that are already minimally represented
        if len(ref) == 1 and len(alt) == 1 and ref in ['A', 'C', 'G', 'T'] and alt in ['A', 'C', 'G', 'T']:
            return [chrom, pos, '.', ref, alt]
        # This first loop left-aligns and removes excess nucleotides on the right.
        # This is Algorithm 1 lines 1-6 from Tan et al 2015
        keep_working = True
        while keep_working:
            keep_working = False
            if len(ref) > 0 and len(alt) > 0 and ref[-1] == alt[-1]:
                ref = ref[:-1]
                alt = alt[:-1]
                keep_working = True
            if len(ref) == 0 or len(alt) == 0:
                preceding_base = pysam_fasta.fetch(chrom, pos - 2, pos - 1)
                ref = preceding_base + ref
                alt = preceding_base + alt
                pos = pos - 1
                keep_working = True
        # This second loop removes excess nucleotides on the left. This is Algorithm 1 lines 7-8.
        while len(ref) > 1 and len(alt) > 1 and ref[0] == alt[0]:
            ref = ref[1:]
            alt = alt[1:]
            pos = pos + 1
        return [chrom, pos, '.', ref, alt]

    normalized = []
    for r in results_list:
        if type(r[0]) is list:
            for l in r:
                if type(l[0]) is list:
                    for t in l:
                        normalized_results = norm(pysam_fasta=pysam_reference,
                                                  chrom=reference_header,
                                                  pos=t[1],
                                                  ref=t[3],
                                                  alt=t[4])
                        # Adding the last columns to the record
                        # and appending to out list
                        normalized.append(normalized_results + t[5:])
                else:
                    normalized_results = norm(pysam_fasta=pysam_reference,
                                              chrom=reference_header,
                                              pos=l[1],
                                              ref=l[3],
                                              alt=l[4])
                    # Adding the last columns to the record
                    # and appending to out list
                    normalized.append(normalized_results + l[5:])
        else:
            normalized_results = norm(pysam_fasta=pysam_reference,
                                      chrom=reference_header,
                                      pos=r[1],
                                      ref=r[3],
                                      alt=r[4])
            # Adding the last columns to the record
            # and appending to out list
            normalized.append(normalized_results + r[5:])
    return normalized


def set_consecutive_entries(record_list):
    """
    Groups consecutive variants into lists contained within a list

    :param record_list: list of variants
    :return: list of variants that are consecutive and
             list of variants that are not consecutive
    """
    logger = logging.getLogger('FindConsecutiveEntries')
    logger.debug('Identifying consecutive variants')
    vcf_out = []  # List of records which we will write to the new vcf file
    consecutives, duplicates = [], []
    block, intermediate_list = [],[]
    distance, last_position = 0, 0
    duplicate = False
    for record in record_list:
        if "PASS" in record[6] or 'HETERO' in record[6]:
            distance = abs(int(record[1]) - int(last_position))
            if last_position == 0:
                last_position = int(record[1])
                last_record = record
                block.append(record)
                continue
            # Hit duplicate entry, reset block and add block
            if distance == 0:
                duplicate = True
                block.append(last_record)
                block.append(record)
            # Hit non-consecutive entry or the first entry of a new block,
            # reset block and add block
            elif distance > 1:
                # First entry in entire file, skip
                if not vcf_out:
                    vcf_out.append(record)
                    continue
                else:
                    if block:
                        if duplicate is True:
                            new_blocks, single_records = checkDuplicates(block)
                            vcf_out += single_records
                            consecutives.append(new_blocks)
                            duplicate = False
                        # Only add the block to process if the length is greater than one
                        # Extend consecutive list
                        elif len(block) > 1:
                            consecutives.append(block)
                        # Write out single record to vcf file
                        elif len(block) == 1:
                            vcf_out.append(block[0])
                        block = []
                        block.append(record)
                    else:
                        block.append(record)
            # Consecutive entries, append previous and
            # current record to current block
            elif distance == 1:
                if last_record not in block:
                    block.append(last_record)
                if last_record in vcf_out:
                    vcf_out.remove(last_record)
                if record in vcf_out:
                    vcf_out.remove(record)
                block.append(record)
            else:
                logger.warning('[BUG] Distance is negative! Distance was ' + str(distance))
            last_position = int(record[1])
            last_record = record
        elif 'NO' in record[6] or "LOW" in record[6]:
            vcf_out.append(record)
    # Last block
    if (len(block) > 1) and (duplicate is False):
        block_set = set(map(tuple, block))
        block_unique = list(map(list, block_set))
        consecutives.append(block_unique)
    elif (len(block) > 1) and (duplicate is True):
        vcf_out += block
    elif len(block) == 1:
        vcf_out.append(block[0])
    elif len(block) == 0:
        pass
    return consecutives, vcf_out  # Return list of consec records and list of consecutive records


def check_type(entry):
    """
    Checks the variant type of a given variant line
    :param entry: variant line
    :return: type of variant (SNP, insertion, deletion)
    """
    logger = logging.getLogger('CheckType')
    ref = entry[3]
    alt = entry[4]

    if len(ref)==len(alt):
        return "snp", 0
    if len(ref)<len(alt):
        insert_len=len(alt)-len(ref)
        return "insertion", insert_len
    if len(ref)>len(alt):
        delete_len=len(ref)-len(alt)
        return "deletion", delete_len
    else:
        logger.warning('Type of variant not found')


def populate_list(ref_alt, pos):
    """
    Bases and positions in variant line

    :param ref_alt: variant line
    :param pos: position
    :return: list of base, position
    """
    ref_alt_list = []
    for i, base in enumerate(ref_alt):
        ref_alt_list.append((base, i + int(pos)))
    return ref_alt_list


def contains_del_snp(group):
    """
    Identifies a deletion/SNP

    :param group: VCF record
    :return: integer
    """
    index = -1
    prev_type = ""
    for i, item in enumerate(group):
        cur_type = check_type(item)[0]
        if (cur_type == "snp") and (prev_type == "deletion"):
            index = i - 1
            return index
        prev_type = check_type(item)[0]
    return index


def contains_consec(group, snp_or_del):
    """
    Checks if the group of consecutive variants passed in has consecutive snps,
    returns if there are either consecutive snps or dels,
    the index of the first occurrence and the length

    :param group: VCF records
    :param snp_or_del: string, 'snp', 'del'
    :return: index and length
    """
    contains_consec_index = -1 # Index representing start of consec snps/dels
    length = -1 # Number of consecutive snps/dels counted in total
    snp_counts = 1
    del_counts = 1
    prev_type = "" # simulates parent pointer
    prev_entry, cur_entry = None,None

    for index, cur_entry in enumerate(group):
        cur_type = check_type(cur_entry)[0]

        # First entry in group
        if index == 0:
            prev_type = cur_type
            prev_entry = cur_entry
        else:
            # Found at least 2 consecutive entries that must be snp/snp or del/del
            if (cur_type == snp_or_del) and (prev_type == snp_or_del):
                next_pos = int(prev_entry[1]) + 1
                # Increase counts only if positions are also consecutive
                if int(cur_entry[1]) == next_pos:
                    if prev_type == "snp":
                        snp_counts += 1
                        if contains_consec_index == -1:
                            # Set start index of consec snps
                            contains_consec_index = index - 1

                    elif prev_type == "deletion":
                        del_counts += 1
                        if contains_consec_index == -1:
                            # Set start index of consec dels
                            contains_consec_index = index - 1

            elif prev_type != cur_type or index == (len(group) - 1):
                if (snp_counts > 1) and (snp_or_del == "snp"):
                    length = snp_counts
                    return contains_consec_index, length
                elif (del_counts > 1) and (snp_or_del == "deletion"):
                    length = del_counts
                    return contains_consec_index, length

        # Update parent pointer to last current entry
        prev_entry = cur_entry
        prev_type = check_type(prev_entry)[0]
    if length == -1:
        if snp_counts > 1 and snp_or_del == 'snp':
            length = snp_counts
        elif del_counts > 1 and snp_or_del == 'deletion':
            length = del_counts
    # No consecutive snps or dels found
    return contains_consec_index, length


def ins_del(group):
    """
    Checks if the current group of consecutive entries has
    contains either a ins/del or del/ins

    :param group: VCF records
    :return: index of first occurrence of ins/del or del/ins
    """
    prev_entry_type, cur_entry_type = "",""
    for index,entry in enumerate(group):
        cur_entry_type = check_type(entry)[0]
        if index != 0:
            if (cur_entry_type == "deletion") and (prev_entry_type == "insertion"):
                return index-1,"ins_del"
            elif (cur_entry_type == "insertion") and (prev_entry_type == "deletion"):
                return index-1,"del_ins"
        prev_entry_type = cur_entry_type
    return -1, None  # No ins_del or del_ins found


def checkDuplicates(consecutive_block):
    """
    Checks for duplicates in the current consecutive block

    :param consecutive_block: blocks of variant records
    :return: list of new singleton variants, list of updated consecutive blocks
             and
    """
    logger = logging.getLogger('CheckDuplicates')
    consecutive_block = [list(x) for x in set(tuple(x) for x in consecutive_block)]
    if len(consecutive_block) == 1:
        return [], consecutive_block
    true_duplicates = []
    last_pos = 0
    previous_record = ''
    for current_record in consecutive_block:
        if last_pos == 0:
            previous_record = current_record
            last_pos = int(current_record[1])
            continue
        if previous_record:
            if ','.join([str(i) for i in previous_record]) == ','.join([str(i) for i in current_record]):
                previous_record = current_record
                continue
        current_alt = str(current_record[4])
        previous_alt = str(previous_record[4])
        current_ref = current_record[3]
        previous_ref = previous_record[3]
        current_qual = float(current_record[5])
        last_qual = float(previous_record[5])
        current_filter = current_record[6]
        previous_filter = previous_record[6]
        current_record_joined = ','.join([str(i) for i in current_record])
        previous_record_joined = ','.join([str(i) for i in previous_record])
        add_previous = False
        add_current = False
        if int(current_record[1]) - last_pos == 0:
            # If it's a SNP/DEL or DEL/SNP duplicate, these are not true duplicates
            if (check_type(current_record) == 'snp' and check_type(previous_record) == 'deletion') or \
               (check_type(current_record) == 'deletion' and check_type(previous_record) == 'snp'):
                continue
            # If it's two multibase insertions, pick the variant with the highest quality
            elif check_type(current_record) == 'insertion' and check_type(previous_record) == 'insertion':
                if (len(previous_alt) > len(current_alt) and len(current_alt) > 2) or \
                   (len(current_alt) > len(previous_alt) and len(previous_alt) > 2):
                    if last_qual > current_record[5]:
                        add_current = True
                    elif previous_record_joined not in true_duplicates:
                        add_previous = True
            # If it's two multibase deletions, pick the variant with the highest quality
            elif check_type(current_record) == 'deletion' and check_type(previous_record) == 'deletion':
                if (len(previous_ref) > len(current_ref) and len(current_ref) > 2) or \
                   (len(current_ref) > len(previous_ref) and len(previous_ref) > 2):
                    if last_qual > current_record[5]:
                        add_current = True
                    elif previous_record_joined not in true_duplicates:
                        add_previous = True
            # If it's a multibase indel/single base indel, take the multibase indel
            elif (len(previous_alt) > len(current_alt) and len(current_alt) == 2) or \
                 (len(previous_ref) > len(current_ref) and len(current_ref) == 2):
                add_current = True
            elif len(current_alt) > len(previous_alt) and len(previous_alt) == 2 or \
                 (len(current_ref) > len(previous_ref) and len(previous_ref) == 2 and
                  previous_record_joined not in true_duplicates):
                add_previous = True
            # If the two entries are exact duplicates
            elif previous_ref == current_ref or previous_alt == current_alt:
                # Take the duplicate that is PASS
                if current_filter == 'PASS' and previous_record_joined not in true_duplicates:
                    add_previous = True
                elif previous_filter == 'PASS' and current_record_joined not in true_duplicates:
                    add_current = True
                # If the FILTERs are the same,
                # take the record with the lowest quality
                # (worst case scenario)
                elif current_qual > last_qual and current_record_joined not in true_duplicates:
                    add_current = True
                elif current_qual < last_qual and previous_record_joined not in true_duplicates:
                    add_previous = True
                # If they are exactly identical,
                # remove the current record (chosen randomly)
                else:
                    add_current = True
            elif len(previous_alt) > len(current_alt) and current_record_joined not in true_duplicates:
                add_current = True
            elif len(previous_alt) < len(current_alt) and previous_record_joined not in true_duplicates:
                add_previous = True
            elif len(previous_ref) > len(current_ref) and current_record_joined not in true_duplicates:
                add_current = True
            elif len(previous_ref) < len(current_ref) and previous_record_joined not in true_duplicates:
                add_previous = True
            elif check_type(previous_record) == 'snp' and check_type(current_record) != 'snp':
                continue
            else:
                logger.info('Unknown case: ')
                logger.info(previous_record)
                logger.info(current_record)
        if add_current is True:
            # previous_minor_vars = previous_record[7].split(';')[-1].split('=')[1]
            # current_minor_vars = current_record[7].split(';')[-1].split('=')[1]
            # if current_minor_vars and previous_minor_vars:
            #     current_record[7] = current_record[7] + ',' + previous_minor_vars
            # elif previous_minor_vars and not current_minor_vars:
            #     current_record[7] += previous_minor_vars
            # current_record_joined = ','.join([str(i) for i in current_record])
            true_duplicates.append(current_record_joined)
        elif add_previous is True:
            # previous_minor_vars = previous_record[7].split(';')[-1].split('=')[1]
            # current_minor_vars = current_record[7].split(';')[-1].split('=')[1]
            # if current_minor_vars and previous_minor_vars:
            #     previous_record[7] = ','.join([previous_record[7], current_minor_vars])
            # elif current_minor_vars and not previous_minor_vars:
            #     previous_record[7] += current_minor_vars
            # previous_record_joined = ','.join([str(i) for i in previous_record])
            true_duplicates.append(previous_record_joined)
        last_pos = int(current_record[1])
        previous_record = current_record
    updated_block = []
    for i in consecutive_block:
        if ','.join([str(j) for j in i]) not in true_duplicates:
            updated_block.append(i)
    previous = ''
    singletons = []
    new_block = []
    all_blocks = []
    if len(updated_block) > 1:
        for record in updated_block:
            if type(previous) is not str:
                if int(record[1]) - int(previous[1]) == 1:
                    if previous in singletons:
                        singletons.remove(previous)
                    new_block.append(record)
                elif int(record[1]) - int(previous[1]) > 1:
                    if previous in new_block:
                        new_block.remove(previous)
                    singletons.append(record)
                    all_blocks.append(new_block)
                    new_block = []
            else:
                singletons.append(record)
                new_block.append(record)
            previous = record
    return new_block, singletons


def combineVariants(consecutive_list):
    """
    Combines variants based on specific criteria
    Consecutive variants are considered:
    DEL/INS, INS/DEL, SNP/SNP*, DEL/DEL*
    Consecutive variants of 3 or more are ignored

    :param consecutive_list: list of variants that are consecutive positions
    :return: merged variants
    """
    logger = logging.getLogger('CombineVariants')
    logger.info("Combining variants")
    compare = lambda x, y: Counter(x) == Counter(y)
    merged_variants = []
    previous_id, current_id = "", ""
    prev_entry = ""
    logger.debug(str(len(consecutive_list)) + ' consecutive blocks ')
    for group in consecutive_list:
        group_set = set(map(tuple, group))
        group_unique = list(map(list, group_set))
        group = sorted(group_unique, key=itemgetter(1))
        if len(group) > 2:
            group_size = len(group)
            snp_count, ins_count, del_count = 0, 0, 0
            unknown_cases, unknown_refs_alts = [], []

            for index, entry in enumerate(group):
                entry_type = check_type(entry)[1]
                if entry_type == "snp":
                    snp_count += 1
                    unknown_cases.append("snp")
                elif entry_type == "insertion":
                    ins_count += 1
                    unknown_cases.append("insertion")
                elif entry_type == "deletion":
                    del_count += 1
                    unknown_cases.append("deletion")
                unknown_refs_alts.append([entry[1], entry[3], str(entry[4])])

            if snp_count == group_size:
                combined_ref, combined_alt  = "",""
                for index,entry in enumerate(group):
                    combined_ref += entry[3]
                    combined_alt += str(entry[4])

                HETERO = False
                filter = ""
                lowest_qual = sys.maxsize
                index = -1  # Position of record in group with lowest quality
                for i,record in enumerate(group):
                    if record[6]:
                        if 'HETERO' in record[6]:
                            filter = record[6]
                            HETERO = True
                        if float(record[5]) < lowest_qual:
                            index = i  # Store index with lowest quality score
                            lowest_qual = float(record[5])
                if HETERO is False:
                    filter = group[0][6]
                # Assign rest of columns from record with lowest quality score using index
                entry = group[index]
                chrom = entry[0]
                pos = group[0][1]  # Set position to first entry to avoid REF_MISMATCH error
                qual = str(lowest_qual) if lowest_qual != sys.maxsize else entry[5]
                info = entry[4]
                vcf_entry = createVCFRecord([pos, combined_ref, combined_alt, qual, filter, info], chrom)
                merged_variants.append(vcf_entry)
                continue  # Go to next group

            elif ins_count > 1:
                logger.debug('Insertions ' + ','.join([str(entry[1]) for entry in group]))
                for item in group:
                    merged_variants.append(item)
                continue

            # All deletions: unique union of refs and only keep first alt base
            elif del_count == group_size:
                logger.debug('Deletions ' + ','.join([str(entry[1]) for entry in group]))
                # Get ref (unique union of all consecutive refs)
                set_refs = []
                combined_refs = ""
                for entry in group:
                    intermediate = populate_list(entry[3], entry[1])
                    set_refs += intermediate
                set_refs = set(set_refs)
                sorted_refs = sorted(set_refs, key=itemgetter(1))  # Sort set of items by position
                for index, entry in enumerate(sorted_refs):
                    combined_refs += entry[0]

                first_entry = group[0]
                combined_alts = [first_entry[2]]  # only the first letter in the tuple will be the new alt

                HETERO = False
                lowest_qual = sys.maxsize
                index = -1  # Position of record in group with lowest quality
                filter = ""
                for i, record in enumerate(group):
                    if record[6]:
                        if 'HETERO' in record[6]:
                            filter = record[6]
                            HETERO = True
                        if float(record[5]) < lowest_qual:
                            index = i
                            lowest_qual = float(record[5])
                if HETERO is False:
                    filter = group[0][6]
                entry = group[index]
                chrom = entry[0]
                pos = entry[1]
                qual = str(lowest_qual) if lowest_qual != sys.maxsize else entry[5]
                info = entry[7]

                vcf_entry = createVCFRecord([pos, combined_refs, combined_alts, qual, filter, info], chrom)
                merged_variants.append(vcf_entry)
                continue

            if contains_consec(group, "snp")[0] != -1:
                logger.debug('Merging all SNPs')
                combined_ref,combined_alt = "", ""
                start_position = contains_consec(group, "snp")[0]  # Keep original copy of start
                start_pos_copy = start_position  # Create copy of start position to modify
                consec_types = contains_consec(group, "snp")[1]  # Stores number of variants of the same type in a row

                for x in range(consec_types):
                    cur_record = group[start_pos_copy]
                    combined_ref += cur_record[3]
                    combined_alt += str(cur_record[4])
                    start_pos_copy += 1  # Go to next consecutive position

                HETERO = False
                lowest_qual = sys.maxsize
                index = -1  # Position of record in group with lowest quality
                filter = ""
                for i, record in enumerate(group):
                    if record[6]:
                        if record[6][0] == "HETERO":
                            filter = record[6]
                            HETERO = True
                        if float(record[5]) < lowest_qual:
                            index = i
                            lowest_qual = float(record[5])
                if not filter:
                    filter = group[0][6]
                entry = group[index]
                chrom = entry[0]
                pos = group[start_position][1]
                qual = str(lowest_qual) if lowest_qual != sys.maxsize else entry[5]
                info = entry[7]
                new_record = createVCFRecord([pos, combined_ref, combined_alt, qual, filter, info], chrom)
                group[start_position:start_position + consec_types] = [new_record]
            if contains_consec(group, "deletion")[0] != -1:
                logger.debug('Merging all Deletions')
                combined_ref, combined_alt = "", ""
                start_position = contains_consec(group, "deletion")[0]  # Keep original copy of start
                start_pos_copy = start_position  # Create copy of start position to modify
                consec_types = contains_consec(group, "deletion")[1]
                first_record = group[start_position]
                combined_alt = first_record[4]
                refs = []  # will hold unique union of all refs in group
                for x in range(consec_types):
                    cur_record = group[start_pos_copy]
                    cur_ref_list = populate_list(cur_record[3], int(cur_record[1]))
                    refs += tuple(cur_ref_list)
                    start_pos_copy += 1
                combined_refs = sorted(set(refs), key=lambda x: x[1])
                for item in combined_refs:
                    combined_ref += item[0]
                first_pos = int(first_record[1])
                HETERO = False
                lowest_qual = sys.maxsize
                index = -1  # Position of record in group with lowest quality
                filter = ""
                for i, record in enumerate(group):
                    if record[6]:
                        if 'HETERO' in record[6]:
                            filter = record[6]
                            HETERO = True
                        if float(record[5]) < lowest_qual:
                            index = i
                            lowest_qual = float(record[5])
                if HETERO is False:
                    filter = group[index][6]
                entry = group[index]
                chrom = entry[0]
                pos = first_pos
                qual = str(lowest_qual) if lowest_qual != sys.maxsize else entry[5]
                info = entry[7]
                new_record = createVCFRecord([pos, combined_ref, combined_alt, qual, filter, info], chrom)
                group[start_position:start_position + consec_types] = [new_record]
            if ins_del(group)[0] != -1:
                if ins_del(group)[1] == "ins_del":
                    logger.debug('Merging Insertion/Deletion ' + ','.join([str(group[ins_del(group)[0]][1]),
                                                                           str(group[ins_del(group)[0] + 1][1])]))
                    # unique union for refs
                    index = ins_del(group)[0]  # index of the ins
                    prev_entry = group[index]
                    ref1 = populate_list(group[index][3], group[index][1])  # Corresponds to the insertion
                    ref2 = populate_list(group[index+1][3], group[index+1][1])  # Corresponds to the deletion
                    alt1 = populate_list(str(group[index][4]), group[index][1])
                    alt2 = populate_list(str(group[index+1][4]), group[index+1][1])
                    combined_ref = sorted(set(ref1+ref2), key=itemgetter(1))
                    combined_alt = sorted(set(alt1+alt2), key=itemgetter(1))
                    new_refs, new_alts = "", ""
                    if len(combined_ref) == len(combined_alt):  # Iterate through list of: [base,position]
                        first_occurence = -1
                        for i, j in zip(combined_ref, combined_alt):
                            if i[0] != j[0]:
                                # store position of first mismatch, make new reference genome position
                                if first_occurence == -1:
                                    new_pos = i[1]
                                    first_occurence += 1
                                new_refs += i[0]
                                new_alts += j[0]
                    else:
                        # lengths of combined ref and alt are not equal, don't merge entire block
                        for record in group:
                            merged_variants.append(record)
                        continue  # Do not check for anymore ins/dels
                    # Search for at least one hetero and index with lowest quality score
                    HETERO = False
                    lowest_qual = sys.maxsize
                    index = -1
                    filter = ""
                    for i,record in enumerate(group):
                        if record[6]:
                            if 'HETERO' in record[6]:
                                filter = record[6]
                                HETERO = True
                            if float(record[5]) < lowest_qual:
                                index = i
                                lowest_qual = float(record[5])

                    if HETERO is False:
                        filter = group[0][6]
                    entry = group[index]
                    chrom = entry[0]
                    pos = new_pos
                    qual = str(lowest_qual) if lowest_qual != sys.maxsize else entry[5]
                    info = entry[7]
                    vcf_entry = createVCFRecord([pos, new_refs, new_alts, qual, filter, info], chrom)
                    group[index:index+2] = [vcf_entry]
                    continue  # Check for more ins/del or del/ins
                elif ins_del(group)[1] == "del_ins":
                    logger.debug('Merging Deletion/Insertion ' + ','.join([str(group[ins_del(group)[0]][1]),
                                                                           str(group[ins_del(group)[0] + 1][1])]))
                    index = ins_del(group)[0]  # index of deletion
                    prev_entry = group[index]
                    ref1 = populate_list(group[index][3], group[index][1])
                    ref2 = populate_list(group[index + 1][3], group[index + 1][1])
                    alt1 = populate_list(str(group[index][4]), group[index][1])
                    alt2 = populate_list(str(group[index + 1 ][4]), group[index + 1][1])
                    combined_ref = sorted(set(ref1+ref2), key=itemgetter(1))  # unique union of populated list
                    combined_alt = alt1  # del's alt populated list
                    for index,entry in enumerate(alt2):
                        if index != 0:
                            combined_alt.append(entry)
                    combined_alt = sorted(set(combined_alt), key=itemgetter(1))
                    if len(combined_ref) == len(combined_alt):
                        new_ref, new_alt, new_pos = "", "", ""
                        first_occurence = -1
                        for i, j in zip(combined_ref, combined_alt):
                            if i[0] != j[0]:
                                if first_occurence == -1:
                                    new_pos = i[1]
                                    first_occurence += 1
                                new_ref += i[0]
                                new_alt += j[0]
                        HETERO = False
                        lowest_qual = sys.maxsize
                        index = -1  # Position of record in group with lowest quality
                        for i, record in enumerate(group):
                            if record[6]:
                                if 'HETERO' in record[6]:
                                    filter = record[6]
                                    HETERO = True
                                if float(record[5]) < lowest_qual:
                                   index = i
                                   lowest_qual = float(record[5])

                        if HETERO == False:
                            filter = group[0][6]
                        entry = group[index]
                        chrom = entry[0]
                        pos = new_pos
                        qual = str(lowest_qual) if lowest_qual != sys.maxsize else entry[5]
                        info = entry[7]
                        vcf_entry = createVCFRecord([pos, new_ref, new_alt, qual, filter, info], chrom)
                        group[index:index+2] = [vcf_entry]
                    else:  # Lengths are not equal --> write out entire block
                        for item in group:
                            merged_variants.append(item)
                        continue  # do not check for anymore del/ins
            if contains_consec(group, "snp")[0] != -1:
                combined_ref, combined_alt = "", ""
                start_position = contains_consec(group, "snp")[0]  # Keep original copy of start
                start_pos_copy = start_position  # Create copy of start position to modify
                consec_types = contains_consec(group,"snp")[1]  # Stores number of variants of the same type in a row
                for x in range(consec_types):
                    cur_record = group[start_pos_copy]
                    combined_ref += cur_record[3]
                    combined_alt += str(cur_record[4])
                    start_pos_copy += 1  # Go to next consecutive position
                HETERO = False
                filter = ""
                lowest_qual = sys.maxsize
                index = -1 # Position of record in group with lowest quality
                for i,record in enumerate(group):
                    if record[6]:
                        if 'HETERO' in record[6]:
                            filter = record[6]
                            HETERO = True
                        if float(record[5]) < lowest_qual:
                            index = i
                            lowest_qual = float(record[5])
                if HETERO == False:
                       filter = group[0][6]
                entry = group[index]
                chrom = entry[0]
                pos = new_pos
                qual = str(lowest_qual) if lowest_qual != sys.maxsize else entry[5]
                info = entry[7]
                new_record = createVCFRecord([pos, combined_ref, combined_alt, qual, filter, info], chrom)
                group[start_position:start_position + consec_types] = [new_record]
            # At this point, all ins/del, del/ins, consecutive snps, and consecutive dels,
            # and are combined into new group
            if len(group) == 3:
                if contains_del_snp(group) != -1:
                    del_index = contains_del_snp(group)
                    del group[del_index]
                elif (compare(unknown_cases, ['snp', 'insertion', 'snp']) == True) or \
                        (compare(unknown_cases, ["insertion", "snp", "deletion"]) == True):
                    concatenated_ref = []
                    concatenated_alt = []
                    for group_record in group:
                        concatenated_ref.append((group_record[3], group_record[1]))
                        concatenated_alt.append((str(group_record[4]), group_record[1]))
                    combined_ref = sorted(set(concatenated_ref), key=itemgetter(1))
                    combined_alt = sorted(set(concatenated_alt), key=itemgetter(1))
                    counter = 0
                    new_refs,new_alts = "",""
                    if len(combined_ref) == len(combined_alt):
                        first_occurence = -1
                        for i,j in zip(combined_ref,combined_alt):
                            if i[0] != j[0]:
                                if first_occurence == -1:
                                    new_pos = i[1]
                                    first_occurence += 1
                                new_refs += i[0]
                                new_alts += j[0]
                    HETERO = False
                    filter = ""
                    lowest_qual = sys.maxsize
                    index = -1  # Position of record in group with lowest quality
                    for i,record in enumerate(group):
                        if record[6]:
                            if 'HETERO' in record[6]:
                                filter = record[6]
                                HETERO = True
                            if float(record[5]) < lowest_qual:
                                index = i
                                lowest_qual = float(record[5])
                    if HETERO == False:
                           filter = group[0][6]
                    entry = group[index]
                    chrom = entry[0]
                    pos = new_pos
                    qual = str(lowest_qual) if lowest_qual != sys.maxsize else entry[5]
                    info = entry[7]
                    vcf_entry = createVCFRecord([pos, new_refs, new_alts, qual, filter, info], chrom)
                    merged_variants.append(vcf_entry)
                    continue  # Next group of consecutive variants
                elif compare(unknown_cases, ['deletion', 'snp', 'insertion']) is True:
                    for item in group:
                        merged_variants.append(item)
                    continue
            elif len(group) == 2:
                # Remain in the same group and leave processing for next set of statements
                pass
            elif len(group) == 1:  # Ex: ['snp','snp','snp'] --> ['snp']
                merged_variants.append(group[0])
                continue  # Iterate to next group of consecutive variants
            elif len(group) == 4:
                combined_refs, combined_alts = "", ""
                for index, record in enumerate(group):  # Simple concatenation
                    combined_refs += record[3]
                    combined_alts += str(record[4])
                HETERO = False
                filter = ""
                lowest_qual = sys.maxsize
                index = -1  # Position of record in group with lowest quality
                for i,record in enumerate(group):
                    if record[6]:
                        if 'HETERO' in record[6]:
                            filter = record[6]
                            HETERO = True
                        if float(record[5]) < lowest_qual:
                            index = i
                            lowest_qual = float(record[5])
                if HETERO == False:
                       filter = group[0][6]
                entry = group[index]
                chrom = entry[0]
                pos = group[0][1]  # Assign position number of first variant in group
                qual = str(lowest_qual) if lowest_qual != sys.maxsize else entry[5]
                info = entry[7]
                vcf_entry = createVCFRecord([pos, combined_refs, combined_alts, qual, filter, info], chrom)
                merged_variants.append(vcf_entry)
                continue
            else:
                logger.warning("unknown case: ", group)
                exit()
        if len(group) == 2:
            for index, cur_entry in enumerate(group):
                if index == 0:  # We are on the first entry of the current consec group
                    previous_id = check_type(cur_entry)[0]
                    prev_entry = cur_entry
                else:
                    current_id = check_type(cur_entry)[0]
                    if previous_id == "insertion":
                        if current_id == "insertion":
                            for item in group:
                                merged_variants.append(item)
                            continue
                        elif current_id == "deletion":
                            ref1 = populate_list(prev_entry[3], prev_entry[1])
                            ref2 = populate_list(cur_entry[3], cur_entry[1])
                            alt1 = populate_list(str(prev_entry[4]), prev_entry[1])
                            alt2 = populate_list(str(cur_entry[4]), cur_entry[1])

                            combined_ref = ref1 + ref2
                            combined_alt = alt1 + alt2
                            new_refs, new_alts= "", ""
                            if len(combined_ref) == len(combined_alt):
                                first_occurence = -1
                                for i, j in zip(combined_ref, combined_alt):
                                    if i[0] != j[0]:
                                        if first_occurence == -1:
                                            new_pos = i[1]
                                            first_occurence += 1
                                        new_refs += i[0]
                                        new_alts += j[0]
                                HETERO = False
                                lowest_qual = sys.maxsize
                                index = -1  # Position of record in group with lowest quality
                                for i, record in enumerate(group):
                                    if record[6]:
                                        if 'HETERO' in record[6]:
                                            filter = record[6]
                                            HETERO = True
                                    if float(record[5]) < lowest_qual:
                                        index = i
                                    lowest_qual = float(record[5])

                                if HETERO == False:
                                    filter = group[0][6]
                                entry = group[index]
                                chrom = entry[0]
                                pos = new_pos
                                qual = str(lowest_qual) if lowest_qual != sys.maxsize else entry[5]
                                info = entry[7]
                                vcf_entry = createVCFRecord([new_pos, new_refs, new_alts, qual, filter, info], chrom)
                                merged_variants.append(vcf_entry)
                        elif current_id == "snp":
                            merged_variants.append(prev_entry)
                            merged_variants.append(cur_entry)
                            continue
                    elif previous_id == "deletion":
                        if current_id == "insertion":
                            ref1 = populate_list(prev_entry[3], prev_entry[1])
                            ref2 = populate_list(cur_entry[3], cur_entry[1])
                            alt1 = populate_list(str(prev_entry[4]), prev_entry[1])
                            alt2 = populate_list(str(cur_entry[4]), cur_entry[1])
                            combined_ref = sorted(set(ref1+ref2), key=itemgetter(1))  # populated list
                            combined_alt = alt1  # populated list
                            for index, entry in enumerate(alt2):
                                if index != 0:
                                    combined_alt.append(entry)
                            combined_alt = sorted(combined_alt, key=itemgetter(1))
                            if len(combined_ref) == len(combined_alt):
                                new_ref, new_alt = "", ""
                                first_occurence = -1
                                for i,j in zip(combined_ref, combined_alt):
                                    if i[0] != j[0]:
                                        if first_occurence == -1:
                                            new_pos = i[1]
                                            first_occurence += 1
                                        new_ref += i[0]
                                        new_alt += j[0]
                                HETERO = False
                                filter = ''
                                lowest_qual = sys.maxsize
                                index = -1  # Position of record in group with lowest quality
                                for i,record in enumerate(group):
                                    if record[6]:
                                        if 'HETERO' in record[6]:
                                            filter = record[6]
                                            HETERO = True
                                    if float(record[5]) < lowest_qual:
                                        index = i
                                        lowest_qual = float(record[5])
                                if HETERO == False:
                                    filter = group[0][6]
                                entry = group[index]
                                chrom = entry[0]
                                pos = new_pos
                                qual = str(lowest_qual) if lowest_qual != sys.maxsize else entry[5]
                                info = entry[7]
                                vcf_entry = createVCFRecord([new_pos, new_ref, new_alt, qual, filter, info], chrom)
                                merged_variants.append(vcf_entry)
                                continue
                            # Lengths are not equal: del/ins can't be merged
                            else:
                                merged_variants.append(prev_entry)
                                merged_variants.append(cur_entry)
                                continue
                        elif current_id == "deletion":
                            ref1_list = populate_list(prev_entry[3], prev_entry[1])
                            ref2_list = populate_list(cur_entry[3], cur_entry[1])
                            set_combined_refs = set(ref1_list + ref2_list)
                            sorted_combined_refs = sorted(set_combined_refs, key=itemgetter(1))
                            combined_refs = ""
                            for unique_union in sorted_combined_refs:
                                combined_refs += unique_union[0]
                            combined_alts = str(prev_entry[4])
                            HETERO = False
                            filter = ''
                            lowest_qual = sys.maxsize
                            index = -1  # Position of record in group with lowest quality
                            for i,record in enumerate(group):
                                if record[6]:
                                    if 'HETERO' in record[6]:
                                        filter = record[6]
                                        HETERO = True
                                if float(record[5]) < lowest_qual:
                                    index = i
                                    lowest_qual = float(record[5])
                            if HETERO == False:
                                filter = group[0][6]
                            entry = group[index]
                            chrom = entry[0]
                            pos = prev_entry[1]
                            qual = str(lowest_qual) if lowest_qual != sys.maxsize else entry[5]
                            info = entry[7]
                            vcf_entry = createVCFRecord([pos, combined_refs, combined_alts, qual, filter, info], chrom)
                            merged_variants.append(vcf_entry)
                            continue
                        elif current_id == "snp":
                            for index, item in enumerate(group):
                                if index != 0:
                                    merged_variants.append(item)
                            continue
                    elif previous_id == "snp":
                        if (current_id == "insertion") or (current_id == "deletion"):
                            for item in group:
                                merged_variants.append(item)
                            continue
                        elif current_id == "snp":
                            if cur_entry[1] == prev_entry[1] + 1:
                                if len(prev_entry[4]) == 1:
                                    prev_alt = prev_entry[4]
                                else:
                                    prev_alt = str(prev_entry[4])
                                if len(cur_entry[4]) == 1:
                                    cur_alt = cur_entry[4]
                                else:
                                    cur_alt = str(cur_entry[4])
                                combined_ref = prev_entry[3] + cur_entry[3]
                                combined_alt = str(prev_alt) + str(cur_alt)
                                HETERO = False
                                lowest_qual = sys.maxsize
                                index = -1  # Position of record in group with lowest quality
                                for i,record in enumerate(group):
                                    if record[6]:
                                        if 'HETERO' in record[6]:
                                            filter = record[6]
                                            HETERO = True
                                    if float(record[5]) < lowest_qual:
                                        index = i
                                        lowest_qual = float(record[5])
                                    if HETERO == False:
                                        filter = group[0][6]
                                entry = group[index]
                                chrom = entry[0]
                                pos = group[0][1]
                                qual = str(lowest_qual) if lowest_qual != sys.maxsize else entry[5]
                                info = entry[7]
                                vcf_entry = createVCFRecord([pos, combined_ref, combined_alt, qual, filter, info],
                                                            chrom)
                                merged_variants.append(vcf_entry)
                            elif cur_entry[1] != prev_entry[1] + 1:
                                for item in group:
                                    merged_variants.append(item)
                                continue
                        previous_id = current_id
                        prev_entry = cur_entry
    return merged_variants


def getTopVariant(variant_coverage, qualities):
    """
    Select a top variant

    :param variant_coverage: coverage of the variant
    :param qualities: quality of all calls
    :return: base
    """
    max_coverage = max(variant_coverage.values())

    top_variants = [call for call, coverage in variant_coverage.items()
                    if coverage == max_coverage]
    top_qualities = {call: quality for call, quality in qualities.items()
                     if call in top_variants}
    if len(top_variants) > 1:
        snps = [var for var in top_variants
                if is_snp(var)]
        single_indels = [var for var in top_variants
                         if is_single_indel(var)]
        multi_indels = [var for var in top_variants
                        if is_multibase_indel(var)]
        if snps:
            max_base_quality = max(top_qualities.values())
            top_snp = [snp for snp in snps
                       if qualities[snp] == max_base_quality]
        else:
            top_snp = None
        if top_snp:
            return top_snp[0]

        score = 0
        top_indel = None
        if multi_indels:
            for indel in multi_indels:
                if qualities[indel] > score:
                    top_indel = indel
                    score = qualities[indel]
        elif single_indels:
            for indel in single_indels:
                if qualities[indel] > score:
                    top_indel = indel
                    score = qualities[indel]

        if top_indel:
            return top_indel
    return top_variants[0]


def getMinorVariants(topVariant, variantQualities):
    minorCalls = {}
    for call, quality in variantQualities.items():
        if call != topVariant and quality > 0:
            minorCalls[call] = quality
    return minorCalls


def calculateAlleleFrequencies(baseCalls, depth):
    return {b: round(d / float(depth), 3) for b, d in baseCalls.items()}


def evaluateVariant(variant, RSR, VSR, var_freq=None):
    """
    Evaluate variants against a reference.

    Given the variant (for which we only really need the type),
    the number of supporting reads for the reference (RSR)
    and the variant (VSR), and the variant frequency,
    return the decision as a VCF filter string (either
    PASS, HETERO, LOW, or NO).

    :param variant: bases
    :param RSR: reference supporting reads
    :param VSR: variant supporting reads
    :param var_freq: variant frequency
    :param multi_del: if multibase deletion
    :return: filter
    """
    if not var_freq:
        var_freq = VSR/(RSR + VSR)

    if VSR < 4:
        if RSR < 4:
            return 'LOW'
        else:
            return 'NO'

    if is_multibase_indel(variant) or is_snp(variant):
        if RSR >= 4 or VSR >= 4:
            if var_freq < (1/3):
                return 'NO'
            elif var_freq <= (2/3):
                return 'HETERO'
            else:
                return 'PASS'
        elif RSR == 0:
            return 'PASS'

    elif is_single_indel(variant):
        if VSR < 6 and RSR < 4:
            return 'LOW'
        elif RSR > 0:
            if var_freq < (1/2) or VSR < 6:
                return 'NO'
            elif var_freq < (2/3):
                return 'HETERO'
            else:
                return 'PASS'
        else:
            return 'PASS'

