#!/usr/bin/env python2

from pbcore.io import AlignmentSet
import numpy as np
import argparse


def viewalignment(input,start,stop):
	f = AlignmentSet(input)

	refStart=start
	refEnd=stop
	windowSize = refEnd - refStart+1

	reads_list = []
	for i in f.readsInRange(f.refNames[0],refStart,refEnd):
		reads_list.append(i)

	baseCallsMatrix = np.zeros(shape=(len(reads_list),windowSize), dtype="S8")


	for i, aln in enumerate(f.readsInRange(f.refNames[0],refStart,refEnd)):
		if aln.MapQV < 20:
			continue
		alnclipped = aln.clippedTo(refStart, refEnd+1)
		#print alnclipped

		readBases = []
		for position in range(start,stop+1):
			if(aln.referenceStart>position or aln.referenceEnd<position):
				readBase=""
			elif(aln.referenceEnd==position):
				readBase   = aln.clippedTo(position-1,position).read(orientation="genomic")
			else:
				readBase   = aln.clippedTo(position,position+2).read(orientation="genomic")[:-1]
			readBases.append(readBase)
		#readBases.append(aln.clippedTo(stop-1,stop).read(orientation="genomic"))
		baseCallsMatrix[i] = readBases

		print alnclipped.reference(orientation="genomic")


	print baseCallsMatrix


if __name__ == "__main__":

	parser=argparse.ArgumentParser()
	parser = argparse.ArgumentParser()
	parser.add_argument('-s','--start', help='start position')
	parser.add_argument('-t','--stop', help='stop position')
	parser.add_argument('-i','--input',help='input file')
	args = parser.parse_args()

	viewalignment(args.input,int(args.start),int(args.stop))
	#viewalignment("/mnt/alborz/share/projects/valafar/data/depot/legacy-gcdd/1-0004/aligned_reads.corrected.cmp.h5",4107741,4107743)
