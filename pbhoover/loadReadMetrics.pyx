import numpy as np
from Bio.Seq import Seq

GAP = '-'

def loadMetrics(read_info_list, interval):
    cdef dict metrics_at_refpos = {}
    for info in read_info_list:
        tStart = info[1][0]
        strand = info[2]
        QV = info[3]
        delQV = info[4]
        referenceRead = referenceFromAlignmentArray(info[0]).decode('UTF-8')
        read = readFromAlignmentArray(info[0]).decode('UTF-8')
        # The H5Read copied code does not reorder a reverse-strand read
        # This performs the reverse for accurate base information for read and reference seq
        if strand:
            read = str(Seq(read[::-1]).complement())
            referenceRead = str(Seq(referenceRead[::-1]).complement())
            QV = QV[::-1]
            delQV = delQV[::-1]
        readLength = len(read)
        position = tStart
        previousInsertedBase = ''
        previousPosition = 0
        previousBase = ''
        readBases = ''
        refBases = ''
        positionsInReadWithInsertion = []
        for i, p in enumerate(range(tStart, tStart + readLength)):
            refbase = referenceRead[i]
            if GAP in refbase or refbase == GAP:
                if previousInsertedBase:
                    base = '+' + previousInsertedBase.lstrip('+') + read[i]
                    insQualityValue += [QV[i]]
                else:
                    base = '+' + read[i]
                    insQualityValue = [QV[i]]
                    if interval[0] <= position - 1 <= interval[1]:
                        positionsInReadWithInsertion.append((position - 1, referenceRead[i - 1], QV[i - 1]))
                position -= 1
                previousInsertedBase = base
            elif read[i] == GAP or GAP in read[i]:
                previousInsertedBase = ''
                base = '-' + referenceRead[i]
                qualityValue = delQV[i]
                # Since the DeletionQV value is stored in the next
                # base call (i.e. not an indel), a while loop is needed
                # to retrieve the DeletionQV
                if qualityValue == 255:
                    j = i
                    while qualityValue == 255:
                        qualityValue = delQV[j]
                        j += 1
            else:
                previousInsertedBase = ''
                base = read[i]
                qualityValue = QV[i]
            if interval[0] <= position <= interval[1]:
                readBases += read[i]
                refBases += referenceRead[i]
                # Got to the end of the insertion so need to add to the dictionary and
                # add the current base
                if previousBase.startswith('+') and not base.startswith('+'):
                    averageQuality = round(float(sum(insQualityValue)) / float(len(insQualityValue)))
                    try:
                        metrics_at_refpos[previousPosition][previousBase].append(averageQuality)
                    except KeyError:
                        try:
                            metrics_at_refpos[previousPosition][previousBase] = [averageQuality]
                        except KeyError:
                            metrics_at_refpos[previousPosition] = {previousBase: [averageQuality]}
                    try:
                        metrics_at_refpos[position][base].append(qualityValue)
                    except KeyError:
                        try:
                            metrics_at_refpos[position][base] = [qualityValue]
                        except KeyError:
                            metrics_at_refpos[position] = {base: [qualityValue]}
                elif base.startswith('+'):
                    pass
                else:
                    try:
                        metrics_at_refpos[position][base].append(qualityValue)
                    except KeyError:
                        try:
                            metrics_at_refpos[position][base] = [qualityValue]
                        except KeyError:
                            metrics_at_refpos[position] = {base: [qualityValue]}
            previousPosition = position
            previousBase = base
            position += 1
        for tup in positionsInReadWithInsertion:
            # In cases where there is a SNP and an insertion in the read
            # at one position, the reference base does not exist in the read
            # Therefore, removal of the reference base cannot be done
            # These cases are skipped in this loop
            try:
                try:
                    metrics_at_refpos[tup[0]][tup[1]].remove(tup[2])
                except ValueError:
                    continue
            except KeyError:
                continue
    return metrics_at_refpos


_basemap = {0b0000: ord("-"),
            0b0001: ord("A"),
            0b0010: ord("C"),
            0b0100: ord("G"),
            0b1000: ord("T"),
            0b1111: ord("N")}
_cBasemap = {0b0000: ord("-"),
             0b0001: ord("T"),
             0b0010: ord("G"),
             0b0100: ord("C"),
             0b1000: ord("A"),
             0b1111: ord("N")}
_basemapArray = np.ndarray(shape=(max(_basemap.keys()) + 1,), dtype=np.byte)
_cBasemapArray = np.ndarray(shape=(max(_basemap.keys()) + 1,), dtype=np.byte)
for (e, v) in _basemap.items():
    _basemapArray[e] = v
for (e, v) in _cBasemap.items():
    _cBasemapArray[e] = v


def referenceFromAlignmentArray(in_array, complement=False):
    """
    author: David Alexander
    modifications: Sarah Ramirez-Busby

    Decode the reference component of an alignment array.

    :param in_array: array to convert to strings
    :param complement: if the complement bases should be considered
    :return: string of bases
    """
    if complement:
        r = _cBasemapArray[in_array & 0b1111]
    else:
        r = _basemapArray[in_array & 0b1111]
    return r.tostring()


def readFromAlignmentArray(in_array, complement=False):
    """
    author: David Alexander
    modifications: Sarah Ramirez-Busby

    Decode the read component of an alignment array.
    returns a string of bases

    :param in_array: array to convert to strings
    :param complement: if the complement bases should be considered
    :return: string of bases
    """
    if complement:
        r = _cBasemapArray[in_array >> 4]
    else:
        r = _basemapArray[in_array >> 4]

    return r.tostring()
