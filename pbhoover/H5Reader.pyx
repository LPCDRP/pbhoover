import numpy as np
import h5py
import bisect


def loadH5(refRange, cmph5, refKey, minimumMapQV):
    refStart = refRange[0]
    refEnd = refRange[1]
    readLocaterByKey = createReadLocatorDict(cmph5)
    # Creates rec_array of ALIGNMENT_INDEX_COLUMNS
    raw_reads = readLocaterByKey[refKey](refStart, refEnd, justIndices=False)
    filtered_reads = [i for i in raw_reads if int(i[13]) >= minimumMapQV]
    offSets = [(i[18], i[19]) for i in filtered_reads]
    readInfoDict = alnInfoDict(h5f=cmph5, offsets=offSets)
    return readLocaterByKey, readInfoDict


def createReadLocatorDict(in_h5):
    """
    author: David Alexander (pbcore/io/CmpH5IO)
    modifications: Sarah Ramirez-Busby
    Retreives alignment information for a given set of positions

    :param in_h5: h5py file object
    :return: dictionary with alignment information
    """
    OFFSET_TABLE_DTYPE = [("ID", np.uint32), ("StartRow", np.uint32), ("EndRow", np.uint32)]

    referenceGroupTbl = np.rec.fromrecords(list(zip(in_h5["RefGroup"]["ID"], in_h5["RefGroup"]["RefInfoID"],
                                                    [path[1:] for path in in_h5["RefGroup"]["Path"]])),
                                           dtype=[('ID', int), ('RefInfoID', int), ('Name', object)])

    referenceInfoTbl = np.rec.fromrecords(list(zip(in_h5["RefInfo"]["ID"],
                                                   in_h5["RefInfo"]["FullName"],
                                                   in_h5["RefInfo"]["Length"],
                                                   in_h5["RefInfo"]["MD5"])),
                                          dtype=[("RefInfoID", int),
                                                 ("FullName" , object),
                                                 ("Length"   , int),
                                                 ("MD5"      , object)])
    refGroupInfoTble = rec_join('RefInfoID',
                                referenceGroupTbl,
                                referenceInfoTbl,
                                jointype='inner')
    offsetTable = in_h5["RefGroup"]["OffsetTable"][()].view(dtype=OFFSET_TABLE_DTYPE).view(np.recarray).flatten()
    # offsetTable = in_h5["RefGroup"]["OffsetTable"]
    joinedreferenceInfoTable = rec_join('ID',
                                        refGroupInfoTble,
                                        offsetTable,
                                        jointype='inner')
    referenceDict = {}
    readLocatorByKey = {}
    for record in joinedreferenceInfoTable:
        if record.ID != -1:
            assert record.ID != record.Name
            shortName = record.FullName.split()[0]
            if (shortName       in referenceDict or
                record.ID       in referenceDict or
                record.Name     in referenceDict or
                record.FullName in referenceDict or
                record.MD5      in referenceDict):
                raise(ValueError, "Duplicate reference contig sequence or identifier")
            else:
                referenceDict[shortName]       = record
                referenceDict[record.ID]       = record
                referenceDict[record.Name]     = record
                referenceDict[record.FullName] = record
                referenceDict[record.MD5]      = record

                readLocator = makeReadLocator(in_h5, record.ID)
                readLocatorByKey[record.ID] = readLocator
                readLocatorByKey[shortName.decode('UTF-8')] = readLocator
    return readLocatorByKey


def alnInfoDict(h5f, offsets):
    h5dict = {}
    inner2_keys_needed = ['AlnArray', 'DeletionQV', 'QualityValue']
    keys_needed_1_level = ['AlnGroup', 'AlnInfo', 'RefGroup', 'RefInfo']
    keys_needed_2_level = ['ref000001']
    OFFSET_TABLE_DTYPE = [("ID", np.uint32), ("StartRow", np.uint32), ("EndRow", np.uint32)]
    ALIGNMENT_INDEX_COLUMNS = ["AlnID", "AlnGroupID", "MovieID", "RefGroupID",
                               "tStart", "tEnd", "RCRefStrand", "HoleNumber",
                               "SetNumber", "StrobeNumber", "MoleculeID",
                               "rStart", "rEnd", "MapQV", "nM", "nMM", "nIns",
                               "nDel", "Offset_begin", "Offset_end", "nBackRead",
                               "nReadOverlap"]
    ALIGNMENT_INDEX_DTYPE = [(COLUMN_NAME, np.uint32)
                             for COLUMN_NAME in ALIGNMENT_INDEX_COLUMNS]
    for key in keys_needed_1_level:
        h5dict[key] = {}
        for inner_key in h5f[key].keys():
            if key == 'RefGroup' and inner_key == 'OffsetTable':
                h5dict[key][inner_key] = h5f["RefGroup"]["OffsetTable"][()].view(dtype=OFFSET_TABLE_DTYPE).\
                    view(np.recarray).flatten()
            elif key == 'AlnInfo' and inner_key == 'AlnIndex':
                rawAlignmentIndex = h5f["AlnInfo"]["AlnIndex"][()]
                alignmentIndex = (rawAlignmentIndex.view(dtype=ALIGNMENT_INDEX_DTYPE)
                                  .view(np.recarray)
                                  .flatten())
                h5dict[key][inner_key] = alignmentIndex
            else:
                h5dict[key][inner_key] = np.array(h5f[key][inner_key])
    for key in keys_needed_2_level:
        h5dict[key] = {}
        for inner_key in h5f[key].keys():
            h5dict[key][inner_key] = {}
            for inner_key2 in h5f[key][inner_key].keys():
                if inner_key2 in inner2_keys_needed:
                    h5dict[key][inner_key][inner_key2] = {}
                    ds = h5f[key][inner_key][inner_key2]
                    for offset in offsets:
                        a = arrayFromDataset(ds, offset[0], offset[1])
                        h5dict[key][inner_key][inner_key2][(offset[0], offset[1])] = a
    return h5dict


def arrayFromDataset(ds, offsetBegin, offsetEnd):
    """
    author: David Alexander

    Extract a one-dimensional array from an HDF5 dataset.

    :param ds: input data
    :param offsetBegin: start of the off set
    :param offsetEnd: end of the off set
    :return: array
    """
    shape = (offsetEnd - offsetBegin,)
    a = np.ndarray(shape=shape, dtype=ds.dtype)
    mspace = h5py.h5s.create_simple(shape)
    fspace = ds.id.get_space()
    fspace.select_hyperslab((offsetBegin,), shape, (1,))
    ds.id.read(mspace, fspace, a)
    return a


def rec_join(key, r1, r2, jointype='inner', defaults=None, r1postfix='1', r2postfix='2'):
    """author: David Alexander (pbcore/io/CmpH5IO)
    modifications: Sarah Ramirez-Busby
    Join record arrays *r1* and *r2* on *key*; *key* is a tuple of
    field names -- if *key* is a string it is assumed to be a single
    attribute name. If *r1* and *r2* have equal values on all the keys
    in the *key* tuple, then their fields will be merged into a new
    record array containing the intersection of the fields of *r1* and
    *r2*.

    *r1* (also *r2*) must not have any duplicate keys.

    The *jointype* keyword can be 'inner', 'outer', 'leftouter'.  To
    do a rightouter join just reverse *r1* and *r2*.

    The *defaults* keyword is a dictionary filled with
    ``{column_name:default_value}`` pairs.

    The keywords *r1postfix* and *r2postfix* are postfixed to column names
    (other than keys) that are both in *r1* and *r2*.
    """
    if isinstance(key, str):
        key = (key, )
    else:
        key = (key + '', )
    for name in key:
        if name not in r1.dtype.names:
            raise ValueError('r1 does not have key field %s'%name)
        if name not in r2.dtype.names:
            raise ValueError('r2 does not have key field %s'%name)

    def makekey(row):
        return tuple([row[name] for name in key])

    r1d = dict([(makekey(row),i) for i,row in enumerate(r1)])
    r2d = dict([(makekey(row),i) for i,row in enumerate(r2)])

    r1keys = set(r1d.keys())
    r2keys = set(r2d.keys())

    common_keys = r1keys & r2keys

    r1ind = np.array([r1d[k] for k in common_keys])
    r2ind = np.array([r2d[k] for k in common_keys])

    common_len = len(common_keys)
    left_len = right_len = 0
    if jointype == "outer" or jointype == "leftouter":
        left_keys = r1keys.difference(r2keys)
        left_ind = np.array([r1d[k] for k in left_keys])
        left_len = len(left_ind)
    if jointype == "outer":
        right_keys = r2keys.difference(r1keys)
        right_ind = np.array([r2d[k] for k in right_keys])
        right_len = len(right_ind)

    def key_desc(name):
        """
        if name is a string key, use the larger size of r1 or r2 before merging
        """
        dt1 = r1.dtype[name]
        if dt1.type != np.string_:
            return (name, dt1.descr[0][1])

        dt2 = r1.dtype[name]
        assert dt2==dt1
        if dt1.num>dt2.num:
            return (name, dt1.descr[0][1])
        else:
            return (name, dt2.descr[0][1])


    keydesc = [key_desc(name) for name in key]

    def mapped_r1field(name):
        """
        The column name in *newrec* that corresponds to the column in *r1*.
        """
        if name in key or name not in r2.dtype.names: return name
        else: return name + r1postfix

    def mapped_r2field(name):
        """
        The column name in *newrec* that corresponds to the column in *r2*.
        """
        if name in key or name not in r1.dtype.names: return name
        else: return name + r2postfix

    r1desc = [(mapped_r1field(desc[0]), desc[1]) for desc in r1.dtype.descr if desc[0] not in key]
    r2desc = [(mapped_r2field(desc[0]), desc[1]) for desc in r2.dtype.descr if desc[0] not in key]
    newdtype = np.dtype(keydesc + r1desc + r2desc)

    newrec = np.recarray((common_len + left_len + right_len,), dtype=newdtype)

    if defaults is not None:
        for thiskey in defaults:
            if thiskey not in newdtype.names:
                raise('rec_join defaults key="%s" not in new dtype names "%s"' % (thiskey, newdtype.names))

    for name in newdtype.names:
        dt = newdtype[name]
        if dt.kind in ('f', 'i'):
            newrec[name] = 0

    if jointype != 'inner' and defaults is not None: # fill in the defaults enmasse
        newrec_fields = newrec.dtype.fields.keys()
        for k, v in defaults.items():
            if k in newrec_fields:
                newrec[k] = v

    for field in r1.dtype.names:
        newfield = mapped_r1field(field)
        if common_len:
            newrec[newfield][:common_len] = r1[field][r1ind]
        if (jointype == "outer" or jointype == "leftouter") and left_len:
            newrec[newfield][common_len:(common_len+left_len)] = r1[field][left_ind]

    for field in r2.dtype.names:
        newfield = mapped_r2field(field)
        if field not in key and common_len:
            newrec[newfield][:common_len] = r2[field][r2ind]
        if jointype == "outer" and right_len:
            newrec[newfield][-right_len:] = r2[field][right_ind]

    newrec.sort(order=key)

    return newrec



def leftmostBinSearch(vec, val):
    """
    author: David Alexander (pbcore/io/CmpH5IO)

    Return the leftmost position in the vector vec of val. If val is
    absent then we return the lefternmost position for the value:
    max(vec[vec < val]). The time complexity here is potentially worse
    than log(n) because of the extra step of walking backwards.
    """
    assert(len(vec) > 0)
    i = bisect.bisect_left(vec, val)

    if i == 0:
        return(i)
    elif i == len(vec):
        v = vec[i-1]
        i -= 1
    else:
        v = vec[i]

    if v > val:
        i -= 1

    while i > 0 and vec[i-1] == vec[i]:
        i -= 1

    return(i)


def rightmostBinSearch(vec, val):
    """
    author: David Alexander (pbcore/io/CmpH5IO)

    Return the rightmost position in the vector vec of val. If val is
    absent then we return the leftmost position of the value:
    min(vec[vec > val]). If val is greater than all elements in vec we
    return len(vec).
    """
    assert(len(vec) > 0)

    i = bisect.bisect_left(vec, val)

    if (len(vec) == i):
        return(i)

    while (i + 1 < len(vec) and vec[i + 1] == val):
        i += 1

    return(i)


def getOverlappingRanges(tStart, tEnd, nBack, nOverlap, rangeStart, rangeEnd):
    """
    author: David Alexander (pbcore/io/CmpH5IO)

    Return indices overlapping the range defined by [rangeStart,
    rangeEnd). Here tStart, tEnd, nBack, nOverlap are vectors of
    length n sorted according to tStart and tEnd. The vectors nBack
    and nOverlap are typically produced by computeIndices[DP].
    """
    assert(rangeEnd > rangeStart and
           len(tStart) == len(tEnd) == len(nBack) == len(nOverlap))

    lM = leftmostBinSearch(tStart, rangeStart)
    lM = lM - nBack[lM]
    rM = rightmostBinSearch(tStart, rangeEnd - .5)

    assert(rM >= lM and rM >= 0 and lM >= 0)

    if (lM == rM):
        return(np.array([], dtype = "uint32"))
    else:
        # We only keep the reads in the range lM .. rM that
        # actually overlap the range, as determined by
        # tEnd > rangeStart
        idxs   = np.arange(lM, rM, dtype = "uint32")   # lM .. rM
        toKeep = tEnd[idxs] > rangeStart
        return(idxs[toKeep])


def makeReadLocator(cmpH5, refSeq):
    """
    author: David Alexander (pbcore/io/CmpH5IO)

    Return a function which can be called iteratively to find reads
    quickly.
    """
    ALIGNMENT_INDEX_COLUMNS = ["AlnID", "AlnGroupID", "MovieID", "RefGroupID",
                               "tStart", "tEnd", "RCRefStrand", "HoleNumber",
                               "SetNumber", "StrobeNumber", "MoleculeID",
                               "rStart", "rEnd", "MapQV", "nM", "nMM", "nIns",
                               "nDel", "Offset_begin", "Offset_end", "nBackRead",
                               "nReadOverlap"]
    ALIGNMENT_INDEX_DTYPE = [(COLUMN_NAME, np.uint32)
                             for COLUMN_NAME in ALIGNMENT_INDEX_COLUMNS]
    rawAlignmentIndex = cmpH5["AlnInfo"]["AlnIndex"][()]
    alignmentIndex = (rawAlignmentIndex.view(dtype=ALIGNMENT_INDEX_DTYPE)
                            .view(np.recarray)
                            .flatten())
    offsets = cmpH5["RefGroup"]["OffsetTable"][()]
    offStart, offEnd = offsets[offsets[:, 0] == refSeq, 1:3].ravel()
    # alignmentIndex = cmpH5["AlnInfo"]["AlnIndex"]
    # offsets = list(cmpH5["RefGroup"]["OffsetTable"][0])
    # offStart = offsets[1]
    # offEnd = offsets[2]

    if offEnd - offStart > 0:
        refAlignIdx = alignmentIndex[offStart:offEnd, ]
        returnEmpty = False
    else:
        refAlignIdx = alignmentIndex[1:2, ]
        returnEmpty = True

    def f(rangeStart, rangeEnd, justIndices = False):
        if returnEmpty:
            idxs = np.array([], dtype = 'uint32')
        else:
            idxs = getOverlappingRanges(refAlignIdx.tStart, refAlignIdx.tEnd,
                                        refAlignIdx.nBackRead, refAlignIdx.nReadOverlap,
                                        rangeStart, rangeEnd)
        if justIndices:
            return(idxs + offStart)
        else:
            return(refAlignIdx[idxs,])
    return f
