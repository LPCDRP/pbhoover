from pbhoover import VariantUtil, loadReadMetrics


class TestLoadReadMetrics:
    def test_loadmetrics(self):
        from numpy import array, uint8
        base_array = [68, 17, 136, 68, 34, 68, 136, 17, 17, 17, 34, 34, 68, 17, 18, 34, 34, 68, 17, 8, 34, 64, 68, 34]
        qv_array = [4, 13, 8, 12, 1, 14, 5, 14, 6, 14, 11, 14, 14, 13, 3, 11, 14, 14, 13, 255, 13, 3, 11, 14, 8]
        delqv_array = [16, 16, 16, 16, 16, 16, 8, 16, 7, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 255, 16, 16, 16, 16,
                       16]
        test_read_info_list = [[array(base_array, dtype=uint8),
                               (6681, 6681 + 24),
                               1,
                               array(qv_array, dtype=uint8),
                               array(delqv_array, dtype=uint8)]]
        test_interval = (6681, 6685)
        test_output = loadReadMetrics.loadMetrics(read_info_list=test_read_info_list,
                                                  interval=test_interval)


class TestVariantUtil:

    def test_calculateconsensusquality(self):
        test_qualities = {'C': [10, 10],
                          '-C': [10, 10, 10, 15]}
        expected_output = {'C': 20.0,
                           '-C': 45.0}
        test_output = VariantUtil.calculateConsensusQuality(in_qualities=test_qualities)
        assert test_output == expected_output

    def test_collapseinsertions(self):
        test_insertions = {'+GGTC': [10, 4], '+GGT': [10, 3, 5, 6, 4], '+GGTCG': [10, 4]}
        expected_insertions_output = {'+GGT': sorted([10, 3, 5, 6, 4, 10, 4, 10, 4])}
        test_insertions_output = VariantUtil.collapse_indels(test_insertions)
        test_insertions_output['+GGT'] = sorted(test_insertions_output['+GGT'])
        assert test_insertions_output == expected_insertions_output

    def test_collapsedeletions(self):
        test_deletions = {'-AAAAA': [10, 10], '-AA': [10, 10, 10, 10, 3], '-AAA': [10]}
        expected_deletions_output = {'-AA': sorted([10, 10, 10, 10, 3, 10, 10, 10])}
        test_deletions_output = VariantUtil.collapse_indels(test_deletions)
        test_deletions_output['-AA'] = sorted(test_deletions_output['-AA'])
        assert test_deletions_output == expected_deletions_output

    def test_consensus(self):
        test_metrics_dict = {2: {'A': [10, 10, 10, 10, 3], '-A': [10, 4]},
                             3: {'+G': [10, 10, 10, 10, 10, 10, 10, 10]},
                             4: {'G': [10, 10, 10], 'T': [10, 10, 10, 15, 5, 5, 10, 13]},
                             5: {'C': [10, 10], '-C': [10, 10, 10, 15, 14, 13, 12, 11, 4, 3]}}

        baseQVDict = test_metrics_dict[2]
        baseCallsDict = {base: len(qv) for base, qv in baseQVDict.items()}
        consensusQV = VariantUtil.calculateConsensusQuality(baseQVDict)
        test_output = VariantUtil.consensus(referencecall='A',
                                            position=2,
                                            calls=baseCallsDict,
                                            qualities=consensusQV)
        freqs = {'A': 0.714, '-A': 0.286}
        INFO = dict(DP=7,
                    AF=','.join([':'.join([base, str(f)]) for base, f in freqs.items()]),
                    MV='')
        expected_output = [2, 'A', '-A', str(consensusQV['-A']) + '0', 'NO', INFO]
        assert test_output == expected_output

    def test_topvaraint_snp(self):
        test_variant_coverage = {'A': 50, '+T': 20, '-GGG': 40}
        test_qualities = {'A': 103.0, '+T': 43.0, '-GGG': 80.0}
        test_topvariant_output = VariantUtil.getTopVariant(variant_coverage=test_variant_coverage,
                                                           qualities=test_qualities)
        expected_output = 'A'
        assert test_topvariant_output == expected_output

    def test_topvaraint_sbi(self):
        test_variant_coverage = {'A': 10, '+T': 40, '-GGG': 30}
        test_qualities = {'A': 30.0, '+T': 98.0, '-GGG': 80.0}
        test_topvariant_output = VariantUtil.getTopVariant(variant_coverage=test_variant_coverage,
                                                           qualities=test_qualities)
        expected_output = '+T'
        assert test_topvariant_output == expected_output

    def test_topvaraint_mbi(self):
        test_variant_coverage = {'A': 10, '+T': 20, '-GGG': 50}
        test_qualities = {'A': 30.0, '+T': 98.0, '-GGG': 130.0}
        test_topvariant_output = VariantUtil.getTopVariant(variant_coverage=test_variant_coverage,
                                                           qualities=test_qualities)
        expected_output = '-GGG'
        assert test_topvariant_output == expected_output

    def test_minorvariants(self):
        test_topvariant = 'A'
        test_variantqualities = {'A': 30.0, '+T': 98.0, '-GGG': 130.0}
        test_output = VariantUtil.getMinorVariants(topVariant=test_topvariant,
                                                   variantQualities=test_variantqualities)
        expected_output = {'+T': 98.0, '-GGG': 130.0}
        assert test_output == expected_output

    def test_allelefrequencies(self):
        test_alleles = {'A': 20, '+T': 10}
        test_dp = sum(test_alleles.values())
        expected_output = {'A': 0.667, '+T': 0.333}
        test_output = VariantUtil.calculateAlleleFrequencies(baseCalls=test_alleles,
                                                             depth=test_dp)
        assert test_output == expected_output

    def test_evaluatevariant_snp_hetero(self):
        # SNP, Hetero
        test_variant = 'A'
        test_rsr = 6
        test_vsr = 6
        test_output = VariantUtil.evaluateVariant(variant=test_variant,
                                                  RSR=test_rsr,
                                                  VSR=test_vsr,
                                                  var_freq=6.0 / 12.0)
        expected_output = 'HETERO'
        assert test_output == expected_output

    def test_evaluatevariant_snp_pass(self):
        # SNP Pass
        test_variant = 'A'
        test_rsr = 6
        test_vsr = 40
        test_output = VariantUtil.evaluateVariant(variant=test_variant,
                                                  RSR=test_rsr,
                                                  VSR=test_vsr,
                                                  var_freq=40.0 / 46.0)
        expected_output = 'PASS'
        assert test_output == expected_output

    def test_evaluatevariant_snp_ref(self):
        # SNP Ref
        test_variant = 'A'
        test_rsr = 30
        test_vsr = 4
        test_output = VariantUtil.evaluateVariant(variant=test_variant,
                                                  RSR=test_rsr,
                                                  VSR=test_vsr,
                                                  var_freq=4.0 / 34.0)
        expected_output = 'NO'
        assert test_output == expected_output

    def test_evaluatevariant_low(self):
        test_variant = 'A'
        test_rsr = 3
        test_vsr = 3
        test_output = VariantUtil.evaluateVariant(variant=test_variant,
                                                  RSR=test_rsr,
                                                  VSR=test_vsr,
                                                  var_freq=3.0 / 6.0)
        expected_output = 'LOW'
        assert test_output == expected_output

    def test_evaluatevariant_sbi_hetero(self):
        test_variant = '-A'
        test_rsr = 7
        test_vsr = 8
        test_output = VariantUtil.evaluateVariant(variant=test_variant,
                                                  RSR=test_rsr,
                                                  VSR=test_vsr,
                                                  var_freq=8.0 / 15.0)
        expected_output = 'HETERO'
        assert test_output == expected_output

    def test_evaluatevariant_sbi_pass(self):
        test_variant = '-A'
        test_rsr = 7
        test_vsr = 20
        test_output = VariantUtil.evaluateVariant(variant=test_variant,
                                                  RSR=test_rsr,
                                                  VSR=test_vsr,
                                                  var_freq=20.0 / 27.0)
        expected_output = 'PASS'
        assert test_output == expected_output

    def test_evaluatevariant_sbi_ref(self):
        test_variant = '-A'
        test_rsr = 20
        test_vsr = 7
        test_output = VariantUtil.evaluateVariant(variant=test_variant,
                                                  RSR=test_rsr,
                                                  VSR=test_vsr,
                                                  var_freq=7.0 / 27.0)
        expected_output = 'NO'
        assert test_output == expected_output

    def test_evaluatevariant_mbi_hetero(self):
        test_variant = '-ATG'
        test_rsr = 7
        test_vsr = 7
        test_output = VariantUtil.evaluateVariant(variant=test_variant,
                                                  RSR=test_rsr,
                                                  VSR=test_vsr,
                                                  var_freq=7.0 / 14.0)
        expected_output = 'HETERO'
        assert test_output == expected_output

    def test_evaluatevariant_mbi_pass(self):
        test_variant = '-ATG'
        test_rsr = 7
        test_vsr = 30
        test_output = VariantUtil.evaluateVariant(variant=test_variant,
                                                  RSR=test_rsr,
                                                  VSR=test_vsr,
                                                  var_freq=30.0 / 37.0)
        expected_output = 'PASS'
        assert test_output == expected_output

    def test_evaluatevariant_mbi_ref(self):
        test_variant = '-ATG'
        test_rsr = 30
        test_vsr = 2
        test_output = VariantUtil.evaluateVariant(variant=test_variant,
                                                  RSR=test_rsr,
                                                  VSR=test_vsr,
                                                  var_freq=2.0 / 32.0)
        expected_output = 'NO'
        assert test_output == expected_output

    def test_findconsecutive_blocks(self):
        test_consecutives = [['1', 2, '.', 'GT', 'G', 40.0, 'PASS'], ['1', 3, '.', 'C', 'CA', 40.0, 'PASS']]
        expected_consecutives_output = test_consecutives
        test_consecutives_output, \
            test_single_records = VariantUtil.set_consecutive_entries(record_list=test_consecutives)
        print(test_consecutives_output)
        # assert test_single_records == [] and test_consecutives_output == expected_consecutives_output

