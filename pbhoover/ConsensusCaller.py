#######################################################################################################################
# PBHoover ConsensusCaller
#
# Calls consensus on a given cmph5 file
#
#######################################################################################################################
import logging

from Bio import SeqIO

from . import VariantUtil,\
              loadReadMetrics

__author__ = 'Sarah Ramirez-Busby'


def callConsensus(in_read_info, referenceFasta):
    """author: Sarah Ramirez-Busby

    Processes an input of read bases, reference bases,
    insertion and deletions to call consensus at each
    position in the region. This is all 0-based indexing!
    VariantUtil.call_consensus() adds 1 to each position
    when writing to VCF

    :param in_read_info: list of lists containing read information
                         returned by the H5reader.readsInRange()
    :param referenceFasta: a string of the input reference FASTA
                           sequence
    :return a list of lists containing pyvcf records
    """
    global GAP
    GAP = '-'
    logger = logging.getLogger('CallConsensus')
    interval = in_read_info[0]
    readInfo = in_read_info[1]
    refSeq, refHeader = referenceSequence(referenceFasta)
    metrics = loadReadMetrics.loadMetrics(read_info_list=readInfo,
                                          interval=interval)
    interval_positions = [p for p in range(interval[0], interval[1])]
    consensus = runConsensus(interval_positions, metrics, refSeq, refHeader.split()[0])
    return [consensus]


def referenceSequence(fasta):
    """
    Parses the input reference FASTA to obtain the header
    and the sequence to call variants against

    :param fasta: FASTA
    :return: sequence and FASTA header
    """
    seq = []
    for record in SeqIO.parse(fasta, 'fasta'):
        seq = record.seq
        header = record.id
    return seq, header


def runConsensus(positions, metrics_dict, reference, CHROM):
    """
    Iterates over all positions in the reference and calls
    consensus at each position using a base and QV
    dictionary

    :param metrics_dict: key: position, value:
                         dictionary: key: base, value: QC
    :param positions: list of reference positions
    :param reference: reference sequence
    :param CHROM: reference header
    :return:
    """
    variant_list = []
    for position in positions:
        currentRef = reference[position]
        previousRef = reference[position - 1]
        try:
            baseQVDict = metrics_dict[position]
        # If there are no reads in this position,
        # create an empty Dictionary.
        # call_consensus will handle the rest
        except KeyError:
            baseQVDict = {}
        # Collapsing indels
        baseQVDict = VariantUtil.collapse_indels(baseQVDict)
        baseCallsDict = {base: len(qv) for base, qv in baseQVDict.items()}
        consensusQV = VariantUtil.calculateConsensusQuality(baseQVDict)
        consensusCall = VariantUtil.consensus(currentRef, position, baseCallsDict, consensusQV)
        if consensusCall[2]:
            # If it's a deletion
            if consensusCall[2].startswith(GAP):
                consensusCall[1] = previousRef + consensusCall[2][1:]  # trimming off the INDEL
                consensusCall[2] = previousRef
            # If it's match/mismatch or insertion
            elif not consensusCall[2].startswith('+'):
                consensusCall[0] = consensusCall[0] + 1
        else:
            consensusCall[2] = '.'
            consensusCall[0] = consensusCall[0] + 1
        vcf_entry = VariantUtil.createVCFRecord(consensusCall, CHROM)
        variant_list.append(vcf_entry)
    return variant_list

