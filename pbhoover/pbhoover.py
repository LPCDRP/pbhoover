#!/usr/bin/env python3
import argparse
import sys
from math import ceil
from functools import partial
from multiprocessing import Pool
import logging
import warnings
import h5py
from Bio import SeqIO
# Local source tree imports
from . import ReadsInRange, \
              VariantUtil, \
              H5Reader, \
              ConsensusCaller

from ._version import version as __version__

__author__ = 'Sarah Ramirez-Busby'


def runPBHoover(consensus, in_h5, output, chunksize, numpro, reference, mapping_quality):
    """
    Executes all modules needed to call consensus/variants
    for a given CmpH5 and reference genome

    :param consensus: call consensus (bool)
    :param in_h5: cmph5 file
    :param output: output VCF file name
    :param chunksize: genome interval sizes to parallel process
    :param numpro: number of processors to use
    :param reference: reference genome (FASTA)
    :param mapping_quality: mapping quality threshold
    :return: None
    """
    # Provide more extensive feedback on the run
    # if --verbose is flagged
    if debug_mode:
        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s:INFO:%(name)s:%(message)s')
    else:
        logging.basicConfig(level=logging.INFO,
                            format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
    logger = logging.getLogger('PBHooverRunner')
    # Ignoring possible numpy warning
    with warnings.catch_warnings():
        warnings.filterwarnings('ignore', message='numpy.dtype size changed, '
                                                  'may indicate binary incompatibility. '
                                                  'Expected 96, got 88')
        warnings.filterwarnings('ignore', message='dataset.value has been '
                                                  'deprecated. Use dataset[()] instead.')

    def referenceSequence():
        seq = []
        for record in SeqIO.parse(reference, 'fasta'):
            seq = record.seq
            header = record.id
        return seq, header

    h5 = h5py.File(in_h5, 'r')

    referenceBases, fastaHeader = referenceSequence()
    referenceID = list(h5['RefInfo']['FullName'][()])[0].decode('UTF-8').split()[0]
    if referenceID != fastaHeader.split()[0]:
        logging.warning('Reference headers don\'t match.')

    logger.info('Preprocessing ' + in_h5)
    readLocatorDict, readInfoDict = H5Reader.loadH5(refRange=(0, len(referenceBases)),
                                           cmph5=h5,
                                           refKey=referenceID,
                                           minimumMapQV=20)
    logger.debug('Getting genome intervals')
    intervals = ReadsInRange.getReadsInterval(h5=readInfoDict,
                                              referenceLength=len(referenceBases),
                                              numberofChunks=int(ceil(len(referenceBases) / chunksize)),
                                              referenceKey=referenceID,
                                              chunkSize=chunksize,
                                              mapQV=mapping_quality,
                                              readLocaterByKey=readLocatorDict)
    logger.debug('Loading read information to memory')
    list_of_reads_list = []
    for chunk in intervals:
        reads_list = ReadsInRange.readsInRange(readLocatorDict=readLocatorDict,
                                               refKey=referenceID,
                                               readInfoDict=readInfoDict,
                                               mapQv=mapping_quality,
                                               refRange=chunk)
        list_of_reads_list.append(reads_list)
    logger.debug('Using ' + str(sys.getsizeof(list_of_reads_list) / 1000.0) + 'Mb of memory')
    logger.info('Calling variants using ' + str(numpro) + ' cores')
    partialCallConsensus = partial(ConsensusCaller.callConsensus,
                                   referenceFasta=reference)
    pool = Pool(numpro)
    results = pool.map(partialCallConsensus,
                       list_of_reads_list)
    pool.close()
    pool.join()
    normalized_results = VariantUtil.normalize(results_list=results,
                                               reference=reference,
                                               reference_header=referenceID,
                                               consensus=consensus)
    records_to_process, single_records = VariantUtil.set_consecutive_entries(normalized_results)
    intermediate_list = VariantUtil.combineVariants(records_to_process) + single_records
    sorted_records = sorted(intermediate_list, key=lambda x: int(x[1]))
    merged_results = []
    # Sort duplicates and have preference for PASS, then HETERO over NO/LOW
    for i, record in enumerate(sorted_records):
        if i != 0:
            # Process duplicate
            if int(record[1]) == int(prev_entry[1]):
                # current record should be switched with current first duplicate
                if not record[6]:
                    sorted_records[i-1],sorted_records[i] = sorted_records[i], sorted_records[i-1]
                elif record[6] and record[6][0] == 'HETERO':
                    sorted_records[i-1], sorted_records[i] = sorted_records[i], sorted_records[i-1]
        prev_entry = record
    sorted_records = [list(x) for x in set(tuple(x) for x in sorted_records)]
    sorted_records = sorted(sorted_records, key=lambda x: int(x[1]))
    merged_results.append(sorted_records)
    logger.info('Writing ' + output)
    VariantUtil.writeVCF(output, merged_results, consensus)
    logger.info('Finished')


def arguments():
    """
    Argument parser

    :return: argparse parse_args()
    """
    parser = argparse.ArgumentParser(description='Variant caller for PacBio RS/RSII data. '
                                                 'Also identifies heterogeneity')
    parser.add_argument('-i',
                        '--cmph5',
                        help='Input cmp.h5 file',
                        required=True)
    parser.add_argument('-r',
                        '--reference',
                        help='Reference FASTA file for indel normalization',
                        required=True)
    parser.add_argument('-c',
                        '--consensus',
                        help='Call consensus (output all positions to VCF). Default is OFF',
                        action='store_true')
    parser.add_argument('-o',
                        '--output',
                        help='Output VCF file name, stdout by default',
                        default=sys.stdout)
    parser.add_argument('-m',
                        '--min-mapqv',
                        dest='mapping_quality',
                        help='Minimum mapping quality to consider a mapped read. Default is 20',
                        default=20,
                        type=int)
    parser.add_argument('-n',
                        '--nproc',
                        type=int,
                        help='number of processors to be used. Default is 1',
                        default=1)
    parser.add_argument('-s',
                        '--chunksize',
                        help='Genome chunk size used for parallelizaton, 5000 by default',
                        type=int,
                        default=5000)
    parser.add_argument('-V',
                        '--verbose',
                        help='Verbose standard output. Writes more information to log',
                        action='store_true')
    parser.add_argument('-v', '--version',
                        help='Print program version and exit',
                        action='store_true')
    return parser.parse_args()


def main():
    args = arguments()
    if args.version:
        print(__version__)
        sys.exit(0)
    global debug_mode
    debug_mode = args.verbose
    runPBHoover(consensus=args.consensus,
                in_h5=args.cmph5,
                output=args.output,
                chunksize=args.chunksize,
                numpro=args.nproc,
                reference=args.reference,
                mapping_quality=args.mapping_quality)


if __name__ == '__main__':
    main()
