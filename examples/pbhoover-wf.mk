space=
space+=
comma=,

INPUTDIR ?= .
VPATH ?= $(INPUTDIR)

basH5_inputs = $(wildcard $(INPUTDIR)/*.bas.h5)

ifneq ($(basH5_inputs),)
inputs := $(notdir $(basH5_inputs))
else
$(error \
No inputs found. Please set INPUTDIR as the path to the directory \
containing your bas.h5 input files)
endif

NPROC ?= $(shell nproc)

REFERENCE ?= reference.fasta

FILTER = filter_plsh5.py
FILTERFLAGS += MinReadScore=0.75
FILTERFLAGS += MinSRL=50
FILTERFLAGS += MinRL=50
FILTERFLAGS := $(subst $(space),$(comma),$(strip $(FILTERFLAGS)))

ALIGNER = blasr
##BLASRFLAGS += -ignoreRegions
ALIGNERFLAGS += -concordant
ALIGNERFLAGS += -useQuality
ALIGNERFLAGS += -randomSeed 1
ALIGNERFLAGS += -bestn 10
ALIGNERFLAGS += -minPctIdentity 70
ALIGNERFLAGS += -minMatch 12
ALIGNERFLAGS += -placeRepeatsRandomly # -hitPolicy randombest
#ALIGNERFLAGS += -minPctAccuracy 75
ALIGNERFLAGS += -minSubreadLength 50
ALIGNERFLAGS += -minReadLength 50

METRICS += DeletionQV
METRICS += IPD
METRICS += InsertionQV
METRICS += PulseWidth
METRICS += QualityValue
METRICS += MergeQV
METRICS += SubstitutionQV
METRICS += DeletionTag
METRICS := $(subst $(space),$(comma),$(strip $(METRICS)))


VARCALLER = pbhoover
# Use this if you want to see variants with low-coverage
#VARCALLERFLAGS += --debug


variants.vcf: aligned_reads.bam
	$(VARCALLER) --reference $(REFERENCE) $(VARCALLERFLAGS) $< > $@

aligned_reads.bam: $(subst .bas.h5,.bam,$(inputs))
	samtools merge -@ $(NPROC) $@ $^
	samtools index $@

# See https://github.com/PacificBiosciences/pbalign/wiki/Tutorial:-How-to-divide-and-conquer-large-datasets-using-pbalign
aligned_reads.cmp.h5: $(subst .bas.h5,.cmp.h5,$(inputs))
	cmph5tools.py merge $^ --outFile $@ --referencesFile $(REFERENCE)
	cmph5tools.py sort --deep $@
	h5repack -f GZIP=1 $@ repacking.cmp.h5 && mv repacking.cmp.h5 $@

unmapped-subreads.fasta: $(subst .bas.h5,.unmapped-subreads.fasta,$(inputs))
	cat $^ > $@

%.sam %.unmapped-subreads.fasta: %.bas.h5 %.rgn.h5
	$(ALIGNER) $< $(REFERENCE) \
	-nproc $(NPROC) \
	-regionTable $*.rgn.h5 \
	-sam \
	-out $@ \
	-unaligned $*.unmapped-subreads.fasta \
	$(ALIGNERFLAGS)

%.rgn.h5: %.bas.h5
	ls $(abspath $<) > $*.fofn
	$(FILTER) --filter='$(FILTERFLAGS)' $*.fofn --outputFofn $*.rgn.fofn
	$(RM) $*.fofn $*.rgn.fofn

filtered_reads.fastq:

.DELETE_ON_ERROR:
.PRECIOUS: %.rgn.h5 aligned_reads.bam filtered_reads.fastq


# Generic conversion rules

.SUFFIXES: .sam .bam .cmp.h5

.sam.bam:
	samtools view -@ $(NPROC) -b -T $(REFERENCE) $< > $@
	samtools sort -@ $(NPROC) $@ SORTED.$@ && mv SORTED.$@* $@

.sam.cmp.h5:
	CigarRoller -o temp.sam $<
	samtoh5 temp.sam $(REFERENCE) $@ -copyQVs
	loadPulses input.bas.h5 $@ -metrics $(METRICS)
	$(RM) temp.sam
