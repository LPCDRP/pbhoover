
# Special Considerations

## Calling variants on specific sequence intervals
PBHoover does not have a way of doing so directly, but you can achieve this by providing an input that only contains alignments to your region of interest.
To prepare this input, you can add intervals to your `samtools view` invocation when preparing the input.
For example, you could change the first line of [the usage example](https://lpcdrp.gitlab.io/pbhoover/#preparing-a-cmph5-alignment-file-from-a-standard-sam-file) to be:

```
samtools sort -O bam aliged-reads.sam | samtools view -F 256 -h 1:300000-400000 > aligned-reads.sorted.primary.sam
```

which would only show you alignments to reference positions 300000-400000 of contig "1".
The rest of the process remains the same.

## Working with older BLASR alignments

PacBio's original read aligner, BLASR, often produces alignments with invalid CIGAR string representations: a mismatch might be shown as a deletion followed by an insertion, for example.
If you are forced to use these alignments rather than produce new ones with a modern aligner, you should use [CigarRoller](https://gitlab.com/LPCDRP/CigarRoller) to minimize loss of reads due to discarding of such alignments.
CigarRoller input is a BAM/SAM file and outputs a BAM/SAM file.
