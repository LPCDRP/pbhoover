SHELL=$(PYTHON)
.ONESHELL:

simple-winner.topvar: coverages={'A':5,'T':4}
simple-winner.topvar: qualities={'A':15, 'T':14}
simple-winner.topvar: result='A'

tied-snps.topvar: coverages={'A':5,'T':5}
tied-snps.topvar: qualities={'A':15, 'T':14}
tied-snps.topvar: result='A'

indels-nosnps.topvar: coverages={'-T':5,'+CCCC':5}
indels-nosnps.topvar: qualities={}
indels-nosnps.topvar: result='+CCCC'

all-kinds.topvar: coverages={'-T':5,'-C':5,'A':5,'+ATGAAA':5,'T':5}
all-kinds.topvar: qualities={'A':15.98, 'T':15.997}
all-kinds.topvar: result='T'

maxqv-not-maxcvg.topvar: coverages={'A': 2, 'C': 1, 'T': 2}
maxqv-not-maxcvg.topvar: qualities={'A': 12.0, 'C': 15.0, 'T': 3.0}
maxqv-not-maxcvg.topvar: result='A'

%.topvar:
	from pbhoover import get_top_variant
	assert get_top_variant($(coverages),$(qualities)) == $(result)

.PHONY: %.topvar
