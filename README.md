# PBHoover
[![install with bioconda](https://img.shields.io/badge/install%20with-bioconda-brightgreen.svg?style=flat)](http://bioconda.github.io/recipes/pbhoover/README.html) 
[![downloads](https://img.shields.io/conda/dn/bioconda/pbhoover.svg?style=flat)](http://bioconda.github.io/recipes/pbhoover/README.html)


pbhoover is a haploid variant calling software that uses a heuristic calling algorithm in order to make base calls with high certainty in low coverage regions.
This software is also capable of mixed population detection with high sensitivity.

## Dependencies
* python
* cython
* h5py
* scikit-bio
* pysam
* Biopython
* numpy
* pytest

## Installation
### Available on Bioconda
```
conda install -c bioconda pbhoover
```
### Using setup.py
```
python3 setup.py install --prefix=~/bin/
```
## Usage
pbhoover 1.x works only with alignments in the legacy [cmp.h5 format](https://pacbiofileformats.readthedocs.io/en/latest/legacy/CmpH5Spec.html).
Adding functionality for standard BAM format files is under construction for the 2.x releases.

### Preparing a cmp.h5 alignment file from a standard SAM file

This workflow is only tested for PacBio data in bas.h5 or bax.h5 format and requires `samtoh5` and `loadPulses`, utilities that used to be distributed with the `blasr` package around the time of SMRTAnalysis 2.3.0.

For reads aligned with `minimap2`, you can do

```
# Sort, then remove secondary alignments ( https://github.com/lh3/minimap2/issues/4 ). They confuse the h5 conversion.
samtools sort -O bam aliged-reads.sam | samtools view -F 256 -h > aligned-reads.sorted.primary.sam
samtoh5 aligned-reads.sorted.primary.sam aliged-reads.sorted.primary.cmp.h5 -smrtTitle
# Add quality value metrics from the raw data file back to the aligned reads file so they are available for use in variant calling
loadPulses <raw data file as fofn, bas.h5, or bax.h5> -metrics DeletionQV,IPD,InsertionQV,PulseWidth,QualityValue,MergeQV,SubstitutionQV,DeletionTag
# Sort again post h5 conversion...
cmph5tools.py sort --deep aliged-reads.sorted.primary.cmp.h5

# optional -- reduce disk usage
h5repack -f GZIP=1 aliged-reads.sorted.primary.cmp.h5  aliged-reads.sorted.primary.repacked.cmp.h5
mv aliged-reads.sorted.primary.repacked.cmp.h5 aliged-reads.sorted.primary.cmp.h5
```

### PBHoover 1.x

```
pbhoover --input aliged-reads.sorted.primary.cmp.h5 --reference reference.fasta -o variants.vcf
```

### PBHoover 2.x (beta)

Example usage of pbhoover for variant calls:
```
pbhoover --cmph5 aligned_reads.cmp.h5 --reference reference.fasta --output variants.vcf
```

To call complete consensus (output all positions):
```
pbhoover --consensus --cmph5 aligned_reads.cmp.h5 --reference reference.fasta --output variants.vcf
```
## Output
Output is in VCF and includes several FILTERs and metrics of the position including:
```
HETERO = there is more than one confident call at that position (either reference and/or multiple variants)
PASS = only one variant call is confident
LOW = too low coverage to call any base
NO = reference base call

AF = allele frequency for all base calls (base: frequency)
MV = minor variant calls (base: consensus QV)
DP = total depth
```
## Citation
Ramirez-Busby, S.M., Elghraoui, A., Kim, Y. Bin, Kim, K. & Valafar, F. PBHoover and CigarRoller: a method for confident haploid variant calling on Pacific Biosciences data and its application to heterogeneous population analysis. bioRxiv (2018). doi:10.1101/360370
